package fr.parkisseo.parkisseo;

import android.Manifest;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentSender;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.location.Location;
import android.net.Uri;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.ActivityCompat;
import android.os.Bundle;

import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.text.Html;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.CycleInterpolator;
import android.widget.NumberPicker;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.*;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.*;

import fr.parkisseo.parkisseo.ControlPark.LoginActivity;
//import fr.parkisseo.parkisseo.ControlPark_old.ui.LoginActivity;
import fr.parkisseo.parkisseo.ParkiCity.Constants;
import fr.parkisseo.parkisseo.ParkiCity.service.PlaceService;
import fr.parkisseo.parkisseo.ParkiCity.service.TimerService;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity implements GoogleMap.OnMyLocationChangeListener, GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener {
    private GoogleMap map;
    private PlaceService placeService;
    private TimerService timerService;
    private Handler handler;
    public static LatLng userLatLng;
    private List<Marker> placeMarkers;
    private Marker carMarker;
    private GoogleApiClient googleApiClient;
    private TextView txt_freeSpaces;
    private SupportMapFragment mapFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        handler = new Handler();

        try
        {
            getSupportActionBar().setTitle(getResources().getString(R.string.name_parkicity));
        }
        catch (Exception error)
        {
            error.printStackTrace();
        }


        Application application = (Application) getApplication();
        placeService = application.getPlaceService();
        timerService = application.getTimerService();

        setContentView(R.layout.activity_main);

        txt_freeSpaces = (TextView) findViewById(R.id.txtFreeSpaces);


        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.M) {
            ActivityCompat.requestPermissions(this,
                    new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                    1);
            mapFragment = (SupportMapFragment) getSupportFragmentManager()
                    .findFragmentById(R.id.map);
        } else {
            mapFragment = (SupportMapFragment) getSupportFragmentManager()
                    .findFragmentById(R.id.map);
            getAsync();
        }


        final FloatingActionButton fabMap = (FloatingActionButton) findViewById(R.id.fabMap);
        fabMap.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (map != null) {
                    if (map.getMapType() == GoogleMap.MAP_TYPE_NORMAL) {
                        fabMap.setImageResource(R.drawable.ic_map_black);
                        map.setMapType(GoogleMap.MAP_TYPE_SATELLITE);
                    } else {
                        fabMap.setImageResource(R.drawable.ic_map_white);
                        map.setMapType(GoogleMap.MAP_TYPE_NORMAL);
                    }
                }
            }
        });

        FloatingActionButton fabContact = (FloatingActionButton) findViewById(R.id.fabContact);
        fabContact.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(Intent.ACTION_VIEW,
                        Uri.parse("mailto:contact@parkisseo.com?subject=Suggestion")));
            }
        });

        FloatingActionButton fabTimer = (FloatingActionButton) findViewById(R.id.fabTimer);
        fabTimer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                pickTimer();
            }
        });


        findViewById(R.id.cancelButton).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                cancelTimer();
            }
        });

        findViewById(R.id.carLocationButton).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                centerOnCarLocation();
            }
        });

        updateTimerPanel();
        checkLocationSettings();
        Refresh();



    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_refresh) {
            Refresh();
            return true;
        }
        else if (id == R.id.action_switchToControlPark)
        {
            Intent intent = new Intent(this, LoginActivity.class);
            startActivity(intent);
            return true;
        }

        //return super.onOptionsItemSelected(item);
        return true;
    }




    //get and instantiate the map
    public void getAsync() {
        mapFragment.getMapAsync(new OnMapReadyCallback() {
            @Override
            public void onMapReady(GoogleMap googleMap) {
                map = googleMap;
                try {
                    map.setMyLocationEnabled(true);
                    map.getUiSettings().setMapToolbarEnabled(true);
                    map.setPadding(16, 16, 16, 185);
                } catch (SecurityException e) {
                    e.printStackTrace();
                }
                map.setOnMyLocationChangeListener(MainActivity.this);
                map.animateCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(43.6, 1.445), 12));
                updateCarMarker();
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        scheduleUpdates();
        handler.post(updateTimerRunnable);
    }

    @Override
    protected void onPause() {
        super.onPause();
        handler.removeCallbacks(updatePlacesRunnable);
        handler.removeCallbacks(updateTimerRunnable);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        googleApiClient.disconnect();
    }

    private void scheduleUpdates() {
        handler.removeCallbacks(updatePlacesRunnable);
        handler.post(updatePlacesRunnable);
    }

    @Override
    public void onMyLocationChange(Location myLoc) {
        Log.d(Constants.TAG, "My location changed " + myLoc);
        boolean firstLoc = userLatLng == null;
        userLatLng = new LatLng(myLoc.getLatitude(), myLoc.getLongitude());
        if (firstLoc) {
            map.animateCamera(CameraUpdateFactory.newLatLngZoom(userLatLng, 16), 200, null);
            scheduleUpdates();
        }
    }


    private void displayPlaces(List<PlaceService.Place> places) {
        if (map != null) {
            if (placeMarkers != null) {
                for (Marker placeMarker : placeMarkers) {
                    placeMarker.remove();
                }
            }

            // BitmapDescriptor freeIcon = BitmapDescriptorFactory.fromResource(R.drawable.pin_free);
            //BitmapDescriptor occupiedIcon = BitmapDescriptorFactory.fromResource(R.drawable.pin_occupied);

            Bitmap freeIconOriginal = BitmapFactory.decodeResource(getResources(),R.drawable.pin_free);
            Bitmap freeIcon = Bitmap.createScaledBitmap(freeIconOriginal, 30, 30, false);

            Bitmap occupiedIconOriginal = BitmapFactory.decodeResource(getResources(),R.drawable.pin_abuse);
            Bitmap occupiedIcon = Bitmap.createScaledBitmap(occupiedIconOriginal, 30, 30, false);


            placeMarkers = new ArrayList<>();
            for (PlaceService.Place p : places) {
                Bitmap icon = p.isFree() ? freeIcon : occupiedIcon;
                Marker marker = map.addMarker(new MarkerOptions()
                        .position(new LatLng(p.latitude, p.longitude))
                        .title(p.name)
                        .snippet(p.getStateDescription())
                        //.icon(icon));
                        .icon(BitmapDescriptorFactory.fromBitmap(icon)));

                placeMarkers.add(marker);
            }
        }
    }



    //This function will calculate the distance between 2 points and will return the distance in meters
    private double getDistanceBetween(double fromLat, double fromLong, double toLat, double toLong) {
        Location from = new Location("locationA");
        from.setLatitude(fromLat);
        from.setLongitude(fromLong);
        Location to = new Location("locationB");
        to.setLatitude(toLat);
        to.setLongitude(toLong);
        double distance = from.distanceTo(to);
        return distance;
    }

    public void pickTimer() {
        if (userLatLng == null) {

            Toast.makeText(this, getResources().getString(R.string.parkicity_getting_location), Toast.LENGTH_SHORT).show();
            return;
        }

        if (timerService.isOngoing()) {
            ViewGroup timerPanel = (ViewGroup) findViewById(R.id.timerPanel);
            timerPanel.animate().translationYBy(20)
                    .setInterpolator(new CycleInterpolator(3))
                    .setDuration(500);

            return;
        }

        final List<Integer> times = new ArrayList<>();
        final List<String> labels = new ArrayList<>();
        labels.add(getResources().getString(R.string.no_alarms));
        for (int i = 5; i <= 60; i += 5) {
            times.add(i);
            labels.add(i + " " + getResources().getString(R.string.minutes));
        }
        final NumberPicker nb = new NumberPicker(this);
        nb.setMinValue(0);
        nb.setMaxValue(labels.size() - 1);
        nb.setValue(3);
        nb.setDisplayedValues(labels.toArray(new String[labels.size()]));
        nb.setDescendantFocusability(NumberPicker.FOCUS_BLOCK_DESCENDANTS);

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(getResources().getString(R.string.parkicity_parkHere))
                .setMessage(getResources().getString(R.string.parkicity_control_parking_time))
                .setView(nb)
                .setPositiveButton(getResources().getString(R.string.validate), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        int index = nb.getValue();
                        Integer delay;
                        if (index > 0) {
                            delay = times.get(index - 1);
                        } else {
                            delay = null;
                        }
                        String label = labels.get(index);
                        Log.d(Constants.TAG, "Delay " + delay);
                        timerService.saveLocationAndTimer(userLatLng, delay * 60);
                        updateCarMarker();
                        updateTimerPanel();

                        String toastText = getResources().getString(R.string.parkicity_parking_memorized);
                        if (delay != null) {
                            toastText += "\n" + getResources().getString(R.string.notified) + label + " \uD83D\uDC4D";
                        }
                        Toast.makeText(MainActivity.this, toastText, Toast.LENGTH_LONG).show();
                    }
                })
                .setNegativeButton(getResources().getString(R.string.cancel), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {

                    }
                })
                .show();
    }


    void Refresh()
    {
        Log.d("RefreshFunction","Running!");
        if (userLatLng != null) {
            placeService.getPlaces(userLatLng.latitude, userLatLng.longitude, 4000000).enqueue(new Callback<List<PlaceService.Place>>() {
                @Override
                public void onResponse(Call<List<PlaceService.Place>> call, Response<List<PlaceService.Place>> response) {
                    List<PlaceService.Place> places = response.body();
                    Log.d(Constants.TAG, "Got places" + places);
                    if (places != null) displayPlaces(places);
                    int freePlacesCount = 0;
                    for (PlaceService.Place p : places) {
                        //calculate the distance and checks if it's free
                        if (getDistanceBetween(userLatLng.latitude, userLatLng.longitude, p.latitude, p.longitude) <= 200 && p.isFree())
                            freePlacesCount++;
                    }
                    //update text
                    String freeSpaceText = getResources().getString(R.string.NumberOfPlaces) + " <b>" + String.valueOf(freePlacesCount) + "</b>";
                    txt_freeSpaces.setText(Html.fromHtml(freeSpaceText));

                }

                @Override
                public void onFailure(Call<List<PlaceService.Place>> call, Throwable t) {
                    Log.e(Constants.TAG, "error", t);
                }
            });
        }
    }

    Runnable updatePlacesRunnable = new Runnable() {
        @Override
        public void run() {
            Log.d("updatePlacesRunnable", "Running!");
            if (userLatLng != null) {
                Log.d(Constants.TAG, "Update places");
                placeService.getPlaces(userLatLng.latitude, userLatLng.longitude, 4000000).enqueue(new Callback<List<PlaceService.Place>>() {
                    @Override
                    public void onResponse(Call<List<PlaceService.Place>> call, Response<List<PlaceService.Place>> response) {
                        List<PlaceService.Place> places = response.body();
                        Log.d(Constants.TAG, "Got places" + places);
                        if (places != null) displayPlaces(places);
                        int freePlacesCount = 0;
                        Log.d("places JSON", places.toString());
                        for (PlaceService.Place p : places) {
                            //calculate the distance and checks if it's free
                            if (getDistanceBetween(userLatLng.latitude, userLatLng.longitude, p.latitude, p.longitude) <= 200 && p.isFree())
                                freePlacesCount++;
                        }
                        //update text
                        String freeSpaceText = getResources().getString(R.string.NumberOfPlaces) +" <b>" + String.valueOf(freePlacesCount) + "</b>";
                        txt_freeSpaces.setText(Html.fromHtml(freeSpaceText));
                    }

                    @Override
                    public void onFailure(Call<List<PlaceService.Place>> call, Throwable t) {
                        Log.e(Constants.TAG, "error", t);
                    }
                });
            }
            //the thread will be executed every 5 seconds
            handler.postDelayed(this, 10000);        }
    };

    Runnable updateTimerRunnable = new Runnable() {
        @Override
        public void run() {
            updateRemainingTime();
            handler.postDelayed(this, 500);
        }
    };

    private void removeCarMarker() {
        if (carMarker != null) {
            carMarker.remove();
            carMarker = null;
        }
    }

    private void updateCarMarker() {
        removeCarMarker();
        LatLng carLocation = timerService.getCarLocation();
        if (carLocation != null) {
            carMarker = map.addMarker(new MarkerOptions()
                    .position(carLocation)
                    .title(getResources().getString(R.string.location_of_car))
                    .icon(BitmapDescriptorFactory.fromResource(R.drawable.car_marker)));
        }
    }

    private void updateRemainingTime() {
        TextView timerTextView = (TextView) findViewById(R.id.timerTextView);
        Long timerTime = timerService.getTimerTime();
        if (timerTime != null) {
            int remainingSeconds = (int) (timerTime - System.currentTimeMillis()) / 1000;
            if (remainingSeconds > 0) {
                int minutes = remainingSeconds / 60;
                int seconds = remainingSeconds % 60;
                timerTextView.setText(String.format("%02d:%02d", minutes, seconds));
            } else {
                timerTextView.setText("00:00");
            }
        } else {
            timerTextView.setText("-");
        }
    }

    private void updateTimerPanel() {
        ViewGroup timerPanel = (ViewGroup) findViewById(R.id.timerPanel);
        timerPanel.setVisibility(timerService.isOngoing() ? View.VISIBLE : View.GONE);
    }

    private void cancelTimer() {
        if (timerService.timerNotExpired()) {
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setTitle(getResources().getString(R.string.deactivate_alarm))
                    .setMessage(getResources().getString(R.string.no_longer_need_notification))
                    .setPositiveButton(getResources().getString(R.string.deactivate_alarm), new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            performCancelTimer();
                        }
                    }).setNegativeButton(getResources().getString(R.string.keep_alarm), new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {

                }
            }).show();
        } else {
            performCancelTimer();
        }

    }

    private void performCancelTimer() {
        timerService.cancelTimer();
        updateCarMarker();
        updateTimerPanel();
    }

    private void centerOnCarLocation() {
        LatLng carLocation = timerService.getCarLocation();
        if (map != null && carLocation != null) {
            map.animateCamera(CameraUpdateFactory.newLatLngZoom(carLocation, 16), 200, null);
            scheduleUpdates();
        }
    }

    private void checkLocationSettings() {
        if (googleApiClient != null) return;

        googleApiClient = new GoogleApiClient.Builder(this)
                .addApi(LocationServices.API)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this).build();
        googleApiClient.connect();

        LocationRequest locationRequest = LocationRequest.create();
        locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder()
                .addLocationRequest(locationRequest);
        builder.setAlwaysShow(true);

        PendingResult<LocationSettingsResult> result =
                LocationServices.SettingsApi.checkLocationSettings(googleApiClient, builder.build());
        result.setResultCallback(new ResultCallback<LocationSettingsResult>() {
            @Override
            public void onResult(@NonNull LocationSettingsResult result) {
                final Status status = result.getStatus();
                final LocationSettingsStates state = result.getLocationSettingsStates();
                switch (status.getStatusCode()) {
                    case LocationSettingsStatusCodes.SUCCESS:
                        break;
                    case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                        try {
                            status.startResolutionForResult(MainActivity.this, 1000);
                        } catch (IntentSender.SendIntentException e) {
                            // Ignore the error.
                        }
                        break;
                    case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
                        break;
                }
            }
        });
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {

    }

    @Override
    public void onConnectionSuspended(int i) {

    }
    ///Users/abedshouman/AndroidStudioProjects/ParkCityTest
    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }
    //When requesting permissions
    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        switch (requestCode)
        {
            case 1: //Location Permissions
                if (
                        ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION)
                                == PackageManager.PERMISSION_GRANTED )
                {
                    checkLocationSettings();
                    googleApiClient.connect();
                    //onMapReady(map);
                    getAsync();
                } else {
                    Toast.makeText(this, getResources().getString(R.string.parkicity_location_permission), Toast.LENGTH_SHORT).show();
                }
                break;
            default:
                super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }
}