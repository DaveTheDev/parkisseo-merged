package fr.parkisseo.parkisseo.ParkiCity.service;
import com.google.gson.annotations.SerializedName;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

import java.util.List;

public interface PlaceService {

    class Place {
        public String getStateDescription() {
            if (isFree())
                return "Libre";
            else return "Occupé";
        }

        enum State {
            @SerializedName("free")
            FREE,
            @SerializedName("occupied")
            OCCUPIED,
            @SerializedName("abuse")
            ABUSE;
        }
        public final long id;
        public final String name;
        public final State state;
        public final double latitude;
        public final double longitude;
        public Place(long id, String name, State state, double latitude, double longitude) {
            this.id = id;
            this.name = name;
            this.state = state;
            this.latitude = latitude;
            this.longitude = longitude;
        }

        public boolean isFree() {
            return state == State.FREE;
        }

        @Override
        public String toString() {
            return "Place{" +
                    "id=" + id +
                    ", name='" + name + '\'' +
                    ", state=" + state +
                    ", latitude=" + latitude +
                    ", longitude=" + longitude +
                    '}';
        }
    }

    @GET("/nearbyPlaces")
    public Call<List<Place>> getPlaces(@Query("latitude") double lat, @Query("longitude") double lon, @Query("radius") double radius);
}
