package fr.parkisseo.parkisseo.ParkiCity.service;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import com.google.android.gms.maps.model.LatLng;

public class TimerService {

    public static final String KEY_TIMER_TIME = "timer.timerTime";
    public static final String KEY_CARLOCATION_LAT = "timer.carLocation.lat";
    public static final String KEY_CARLOCATION_LON = "timer.carLocation.lon";
    private final Context context;
    private final AlarmManager alarmManager;
    private final SharedPreferences preferences;

    private Long timerTime;
    private LatLng carLocation;

    public TimerService(Context context) {
        this.context = context;
        alarmManager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);

        preferences = PreferenceManager.getDefaultSharedPreferences(context);
        if (preferences.contains(KEY_TIMER_TIME)) {
            timerTime = preferences.getLong(KEY_TIMER_TIME, 0);
        }

        if (preferences.contains(KEY_CARLOCATION_LAT)) {
            float lat = preferences.getFloat(KEY_CARLOCATION_LAT, 0);
            float lon = preferences.getFloat(KEY_CARLOCATION_LON, 0);
            carLocation = new LatLng(lat, lon);
        }
    }

    private void scheduleTimer(int minutes) {
        PendingIntent pendingIntent = getPendingIntent();
        long time = getScheduleTime(minutes);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            alarmManager.setExact(AlarmManager.RTC_WAKEUP, time, pendingIntent);
        } else {
            alarmManager.set(AlarmManager.RTC_WAKEUP, time, pendingIntent);
        }
        setTimerTime(time);
    }

    protected long getScheduleTime(int minutes) {
        return System.currentTimeMillis() + minutes * 60 * 1000L;
    }

    private PendingIntent getPendingIntent() {
        return PendingIntent.getBroadcast(
                    context,
                    0,
                    new Intent("parkisseo.ACTION_TIMER"),
                    PendingIntent.FLAG_UPDATE_CURRENT);
    }

    public void saveLocationAndTimer(@NonNull LatLng location, @Nullable Integer minutes) {
        setCarLocation(location);
        if (minutes != null) {
            scheduleTimer(minutes);
        }
    }

    public void cancelTimer() {
        alarmManager.cancel(getPendingIntent());
        setCarLocation(null);
        setTimerTime(null);
    }

    public LatLng getCarLocation() {
        return carLocation;
    }

    public Long getTimerTime() {
        return timerTime;
    }

    private void setCarLocation(LatLng carLocation) {
        this.carLocation = carLocation;
        if (carLocation != null) {
            preferences.edit()
                    .putFloat(KEY_CARLOCATION_LAT, (float) carLocation.latitude)
                    .putFloat(KEY_CARLOCATION_LON, (float) carLocation.longitude)
                    .apply();
        } else {
            preferences.edit().remove(KEY_CARLOCATION_LAT).remove(KEY_CARLOCATION_LON).apply();
        }
    }

    private void setTimerTime(Long timerTime) {
        this.timerTime = timerTime;
        if (timerTime != null) {
            preferences.edit().putLong(KEY_TIMER_TIME, timerTime).apply();
        } else {
            preferences.edit().remove(KEY_TIMER_TIME).apply();
        }
    }


    public boolean isOngoing() {
        return carLocation != null || timerTime != null;
    }

    public boolean timerNotExpired() {
        return timerTime != null && (timerTime - System.currentTimeMillis() > 0);
    }
}
