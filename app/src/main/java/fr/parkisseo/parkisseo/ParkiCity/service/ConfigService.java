package fr.parkisseo.parkisseo.ParkiCity.service;

import android.content.Context;
import android.util.Log;

import java.io.InputStream;
import java.util.Properties;

public class ConfigService {

    private final Properties properties;

    public ConfigService(Context context, String path) throws Exception {
        Log.d("ParkisseoApp", "Building props with " + context + " " + path);
        InputStream stream = context.getAssets().open(path);
        try {
            properties = new Properties();
            properties.load(stream);
            Log.d("ParkisseoApp", "Config loaded " + properties);
        } finally {
            stream.close();
        }
    }

    public String getHost() {
        return properties.getProperty("host");
    }
}
