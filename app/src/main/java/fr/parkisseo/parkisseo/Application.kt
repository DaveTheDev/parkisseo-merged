package fr.parkisseo.parkisseo


import android.app.Activity
import android.content.Context
import android.os.Bundle
import android.preference.PreferenceManager
import android.support.multidex.MultiDex
import com.beincognito.beincognito.services.DeviceWebService
import com.beincognito.beincognito.services.PushRegistrationService
import com.github.salomonbrys.kodein.*
import com.patloew.rxlocation.RxLocation
import com.squareup.moshi.Moshi
import fr.parkisseo.parkisseo.ControlPark_old.services.*
import fr.parkisseo.parkisseo.ControlPark_old.ui.PlaceListViewModel
import fr.parkisseo.parkisseo.ControlPark_old.ui.PlacesMapViewModel
import fr.parkisseo.parkisseo.ParkiCity.service.ConfigService
import fr.parkisseo.parkisseo.ParkiCity.service.PlaceService
import fr.parkisseo.parkisseo.ParkiCity.service.TimerService
import io.reactivex.Observable
import io.reactivex.disposables.Disposable
import io.reactivex.plugins.RxJavaPlugins
import io.reactivex.subjects.BehaviorSubject
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.converter.moshi.MoshiConverterFactory
import timber.log.Timber
import io.reactivex.functions.BiFunction

import fr.parkisseo.parkisseo.ParkiCity.*

typealias MyApp = fr.parkisseo.parkisseo.Application

class Application : android.app.Application() {
    private var configService: ConfigService? = null
    private var retrofit: Retrofit? = null
    internal var placeService: PlaceService? = null
    internal var timerService: TimerService? = null


    @Synchronized
    protected fun getConfigService(): ConfigService {
        if (configService == null) {
            val path = if (BuildConfig.DEBUG) "conf/dev.properties" else "conf/prod.properties"
            try {
                configService = ConfigService(this, path)
            } catch (e: Exception) {
                throw RuntimeException(e)
            }

        }
        return configService as ConfigService
    }

    @Synchronized
    protected fun getRetrofit(): Retrofit? {
        if (retrofit == null) {
            retrofit = Retrofit.Builder()
                    .baseUrl(getConfigService().host)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build()
        }
        return retrofit
    }

    @Synchronized
    fun getPlaceService(): PlaceService? {
        if (placeService == null) {
            placeService = getRetrofit()!!.create(PlaceService::class.java)
        }
        return placeService
    }

    fun getTimerService(): TimerService {
        if (timerService == null) {
            if (BuildConfig.DEBUG) {
                //In debug mode the timer service schedule in seconds not in minutes
                timerService = object : TimerService(this) {
                    override fun getScheduleTime(minutes: Int): Long {
                        return System.currentTimeMillis() + minutes * 1000L
                    }
                }
            } else {
                timerService = TimerService(this)
            }
        }
        return timerService as TimerService
    }
    override fun attachBaseContext(base: Context?) {
        super.attachBaseContext(base)
        MultiDex.install(this)
        RxJavaPlugins.setErrorHandler { e ->
            Timber.e(e, "Undeliverable error")
        }
    }


    //*************Control Park***************



//    override val kodein by Kodein.lazy {
//        bind<MyApp>() with instance(this@Application)
//
//        bind<Configuration>() with singleton {
//            //Configuration("https://parkisseo-node.azurewebsites.net")
//            Configuration("https://dev-api.parkisseo.com")
//        }
//
//        bind<UserService>() with singleton {
//            val prefs = PreferenceManager.getDefaultSharedPreferences(instance<MyApp>())
//            UserService(prefs)
//        }
//
//        bind<UserPreferences>() with singleton {
//            val prefs = PreferenceManager.getDefaultSharedPreferences(instance<MyApp>())
//            UserPreferences(prefs)
//        }
//
//        bind<Retrofit>("noauth") with singleton {
//            Retrofit.Builder()
//                    .baseUrl(instance<Configuration>().baseUrl)
//                    .addConverterFactory(MoshiConverterFactory.create())
//                    .addCallAdapterFactory(RxJava2CallAdapterFactory.createAsync())
//                    .build()
//        }
//
//        bind<LoginWebService>() with singleton {
//            instance<Retrofit>("noauth").create(LoginWebService::class.java)
//        }
//
//        bind<LoginService>() with singleton { LoginService(instance(), instance(), instance()) }
//
//        bind<Retrofit>("auth") with singleton {
//            val userService = instance<UserService>()
//            val httpClient = OkHttpClient.Builder().addInterceptor {
//                val credentials = userService.credentials?.toBasicAuth()
//                if (credentials != null) {
//                    Timber.wtf("credentials: " + userService.credentials);
//                    val request = it.request().newBuilder().header("Authorization", credentials).build()
//                    it.proceed(request)
//
//                } else {
//
//                    Timber.wtf("Not logged in")
//                    it.proceed(it.request())
//                }
//            }.build()
//
//            Retrofit.Builder()
//                    .baseUrl(instance<Configuration>().baseUrl)
//                    .addConverterFactory(MoshiConverterFactory.create(
//                            Moshi.Builder().add(DateAdapter()).build()
//                    ))
//                    .addCallAdapterFactory(RxJava2CallAdapterFactory.createAsync())
//                    .client(httpClient)
//                    .build()
//        }
//
//        bind<RxLocation>() with singleton { RxLocation(instance<MyApp>()) }
//        bind<PlaceWebService>() with singleton { instance<Retrofit>("auth").create(PlaceWebService::class.java) }
//        bind<fr.parkisseo.parkisseo.ControlPark_old.services.PlaceService>() with singleton { PlaceService(instance(), instance()) }
//
//
//        bind<PlaceListViewModel>() with singleton { PlaceListViewModel(instance(), instance()) }
//        bind<PlacesMapViewModel>() with singleton { PlacesMapViewModel(instance(), instance()) }
//
//        bind<DeviceWebService>() with singleton { instance<Retrofit>("auth").create(DeviceWebService::class.java) }
//        bind<PushRegistrationService>() with eagerSingleton {
//            PushRegistrationService(instance<MyApp>(), instance(), instance())
//        }
//
//        bind<SettingsWebService>() with singleton { instance<Retrofit>("auth").create(SettingsWebService::class.java) }
//        bind<SettingsService>() with eagerSingleton {
//            SettingsService(instance(), instance())
//        }
//
//        bind<ScanBleService>() with singleton { ScanBleService(instance<MyApp>(), instance()) }
//
//        bind<PlaceServiceSubscriber>() with singleton { PlaceServiceSubscriber(instance(), instance()) }
//    }


    var activityCount = 0
        set(value) {
            field = value
//            val placeServiceSubscriber = kodein.instance<PlaceServiceSubscriber>()
//            if (field > 0) {
//                placeServiceSubscriber.isForeground.onNext(true)
//            } else {
//                placeServiceSubscriber.isForeground.onNext(false)
//            }
        }




    override fun onCreate() {
        super.onCreate()

        if (BuildConfig.DEBUG) {
            Timber.plant(Timber.DebugTree())
        }
        Timber.i("Application created")

        registerActivityLifecycleCallbacks(object : ActivityLifecycleCallbacks {
            override fun onActivityPaused(p0: Activity?) {

            }

            override fun onActivityResumed(p0: Activity?) {

            }

            override fun onActivityStarted(p0: Activity?) {
                activityCount += 1

            }

            override fun onActivityDestroyed(p0: Activity?) {
            }

            override fun onActivitySaveInstanceState(p0: Activity?, p1: Bundle?) {
            }

            override fun onActivityStopped(p0: Activity?) {
                activityCount -= 1
            }

            override fun onActivityCreated(p0: Activity?, p1: Bundle?) {
            }
        })
    }


}

private class PlaceServiceSubscriber(val placeService: fr.parkisseo.parkisseo.ControlPark_old.services.PlaceService,
                                     userService: UserService) {

    val isForeground = BehaviorSubject.createDefault(false)

    init {
        Observable.combineLatest(isForeground, userService.observeLogin,
                BiFunction<Boolean, Boolean, Boolean> { isForeground, isLoggedIn ->
                    isForeground && isLoggedIn
                })
                .subscribe {
                    if (it) subscribe() else unsubscribe()
                }
    }

    private var subscription: Disposable? = null
    private fun subscribe() {
        if (subscription == null) {
            subscription = placeService.managedPlaces.subscribe()
        }
    }

    private fun unsubscribe() {
        val connection = this.subscription
        if (connection != null) {
            connection.dispose()
            this.subscription = null
        }
    }
}
