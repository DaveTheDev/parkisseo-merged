package fr.parkisseo.parkisseo.ControlPark_old.services

import io.reactivex.Observable
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.subjects.BehaviorSubject
import okhttp3.ResponseBody
import retrofit2.http.Body
import retrofit2.http.GET
import retrofit2.http.POST
import timber.log.Timber
import java.util.concurrent.TimeUnit

data class UserSettings(val notifEnabled: Boolean,
                        val bleAdmin: Boolean)


data class SetUserSettings(val notifEnabled: Boolean)

interface SettingsWebService {

    @GET("/user/settings")
    fun getUserSettings(): Single<UserSettings>

    @POST("/user/settings")
    fun setUserSettings(@Body settings: SetUserSettings): Single<ResponseBody>
}



class SettingsService(val ws: SettingsWebService, userService: UserService) {

    sealed class Operation {
        object Get : Operation()
        data class SetNotification(val enabled: Boolean) : Operation()
    }

    val operations = BehaviorSubject.createDefault<Operation>(Operation.Get)
    val settings: Observable<UserSettings>
    val loading: Observable<Boolean>
    init {
        loading = BehaviorSubject.createDefault(false)
        settings = operations
                .doOnNext { loading.onNext(true) }
                .switchMap { op ->
                    when(op) {
                        Operation.Get -> _settings().toObservable()
                        is Operation.SetNotification ->
                            _setSettings(op.enabled)
                                    .flatMap { _settings() }
                                    .toObservable()
                    }
                }.observeOn(AndroidSchedulers.mainThread())
                .doOnNext { loading.onNext(false) }
                .replay(1)

        val connections = CompositeDisposable()
        userService.observeLogin.distinctUntilChanged().subscribe { loggedIn ->
            if (loggedIn) {
                Timber.i("Logged in, activating background updates")
                settings.connect()
            } else {
                Timber.i("Logged out, deactivating background updates")
                connections.clear()
            }
        }

    }

    fun updateNotification(enabled: Boolean) {
        operations.onNext(Operation.SetNotification(enabled))
    }

    private fun _settings(): Single<UserSettings> {
        return ws.getUserSettings()
                .retryWhen { it.delay(3, TimeUnit.SECONDS) }
                .logOperation("Getting user settings")
    }

    private fun _setSettings(notifEnabled: Boolean): Single<ResponseBody> {
        return ws.setUserSettings(SetUserSettings(notifEnabled))
                .retryWhen { it.delay(3, TimeUnit.SECONDS) }
                .logOperation("Setting user settings")
    }
}