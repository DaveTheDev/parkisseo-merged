package fr.parkisseo.parkisseo.ControlPark_old.ui

import android.content.Context

class TermsPref(context:Context){
    val PREF_NAME="term_shared"
    val term_pref="isFirstTime"
    val preferences=context.getSharedPreferences(PREF_NAME,Context.MODE_PRIVATE)

    fun getisFirstTime():Boolean{
        return preferences.getBoolean(term_pref,true)
    }
    fun setisFirstTime(state:Boolean){
        val editor=preferences.edit()
        editor.putBoolean(term_pref,state)
        editor.apply()
    }
}