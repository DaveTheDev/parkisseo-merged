package fr.parkisseo.parkisseo.ControlPark_old.services


import com.beincognito.beincognito.services.PushRegistrationService
import io.reactivex.Completable
import io.reactivex.Single
import retrofit2.http.Body
import retrofit2.http.POST

interface LoginWebService {
    data class LoginDTO(val user: String, val password: String)
    data class LoginResponse(val valid: Boolean)

//    @POST("/user/testLogin")
    @POST("/users/self")
    fun login(@Body dto: LoginDTO): Single<LoginResponse>
}


class LoginService(val ws: LoginWebService, val userService: UserService,


    val pushRegistrationService: PushRegistrationService) {

    fun login(c: Credentials): Single<Boolean> {

        return ws.login(LoginWebService.LoginDTO(c.email, c.password))
                .map {
                    userService.credentials = c
                    it.valid
                }
                .retry(3)
                .logOperation("Login")

    }

    fun logout(): Completable {
        return pushRegistrationService.unregisterDevice().doFinally {
            userService.credentials = null
        }
    }

}