package com.beincognito.beincognito.services

import android.content.Context
import android.provider.Settings
import fr.parkisseo.parkisseo.ControlPark_old.services.disposedBy
import fr.parkisseo.parkisseo.ControlPark_old.services.logOperation
import com.google.android.gms.gcm.GoogleCloudMessaging
import com.google.android.gms.iid.InstanceID
import fr.parkisseo.parkisseo.ControlPark_old.services.UserService
import io.reactivex.Completable
import io.reactivex.Observable
import io.reactivex.Single
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import io.reactivex.subjects.BehaviorSubject
import okhttp3.ResponseBody
import retrofit2.http.Body
import retrofit2.http.POST
import timber.log.Timber
import java.util.concurrent.TimeUnit

/**
 * Created by Alexandre Delattre on 07/04/2017.
 */

sealed class TokenStatus {
    object NoToken : TokenStatus()
    data class SomeToken(val token: String) : TokenStatus()
}



data class Device(val os: String, val deviceId: String, val token: String)
data class DeviceId(val deviceId: String)

interface DeviceWebService {
    @POST("/device/register")
    fun registerDevice(@Body device: Device): Single<ResponseBody>

    @POST("/device/unregister")
    fun unregisterDevice(@Body deviceId: DeviceId): Single<ResponseBody>
}

class PushRegistrationService(val context: Context, userService: UserService, val ws: DeviceWebService) {

    fun getCurrentToken(): Observable<TokenStatus> {
        return Observable.fromCallable {
            val token = InstanceID.getInstance(context)
                    .getToken("868127758207", GoogleCloudMessaging.INSTANCE_ID_SCOPE, null)
            if (token != null) TokenStatus.SomeToken(token) else TokenStatus.NoToken
        }.subscribeOn(Schedulers.io())
    }

    private val updateTokenRequests = BehaviorSubject.createDefault(0)

    init {
        val sendToken = updateTokenRequests.switchMap { getCurrentToken() }.ofType(TokenStatus.SomeToken::class.java).map {
            it.token
        }.doAfterNext { token ->
            Timber.i("Got token and is logged in: $token")
        }.flatMap { token ->
            val id = Settings.Secure.getString(context.contentResolver, Settings.Secure.ANDROID_ID)
            val device = Device("android", id, token)
            ws.registerDevice(device).toObservable()
                    .logOperation("Send push token to server")
                    .retryWhen { it.delay(5, TimeUnit.SECONDS) }
                    .map { }
        }.logOperation("Send token").publish()

        val connections = CompositeDisposable()
        userService.observeLogin.distinctUntilChanged().subscribe { loggedIn ->
            if (loggedIn) {
                Timber.i("Logged in")
                sendToken.connect().disposedBy(connections)
            } else {
                Timber.i("Logged out")
                connections.clear()
            }
        }
    }

    fun updateCurrentToken() {
        updateTokenRequests.onNext(0)
    }

    fun unregisterDevice(): Completable {
        val id = Settings.Secure.getString(context.contentResolver, Settings.Secure.ANDROID_ID)
        return ws.unregisterDevice(DeviceId(id)).logOperation("Unregistering device").toCompletable()
    }
}

fun <A, B, C> combineLatestHelper(obsA: Observable<A>, obsB: Observable<B>, f: (B) -> C): Observable<C> {
    return Observable.combineLatest(obsA, obsB, io.reactivex.functions.BiFunction { _, t2 -> f(t2) })
}