package fr.parkisseo.parkisseo.ControlPark_old.ui

import android.annotation.SuppressLint
import android.app.ProgressDialog
import android.bluetooth.BluetoothDevice
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.MenuItem
import android.widget.Toast
import com.github.salomonbrys.kodein.android.KodeinAppCompatActivity
import com.github.salomonbrys.kodein.instance
import fr.parkisseo.parkisseo.R
import fr.parkisseo.parkisseo.ControlPark_old.services.*
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import kotlinx.android.synthetic.main.activity_place_bledetail.*
import java.util.concurrent.TimeUnit

class PlaceBLEDetailActivity : AppCompatActivity() {
    val disposeBag = CompositeDisposable()
    //val placeService: PlaceService by instance()
    lateinit var service: BLEDeviceService



    @SuppressLint("MissingSuperCall")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_place_bledetail)

        val device = intent.getParcelableExtra<BluetoothDevice>("device")
        title = intent.getStringExtra("title")

        service = BLEDeviceService(this, device)

        buttonWake.setOnClickListener { sendCommand("WAK") }
        buttonSleep.setOnClickListener { sendCommand("SLE") }
        buttonMove.setOnClickListener {
            val place = intent.getSerializableExtra("place") as Place?
            if (place != null) {
                startActivity<MovePlaceActivity>{
                    putExtra("title", title!!)
                    putExtra("place", place)
                }
            } else {
                Toast.makeText(this, getString(R.string.place_not_recognized), Toast.LENGTH_SHORT).show()
            }
        }

        supportActionBar?.setDisplayHomeAsUpEnabled(true)
    }


    override fun onOptionsItemSelected(item: MenuItem) = when (item.itemId) {
        android.R.id.home -> {
            finish()
            true
        }
        else -> false
    }

    private fun sendCommand(cmd: String) {
        val dialog = ProgressDialog.show(this, getString(R.string.loading_term), getString(R.string.sending_order))
        service.writeCommand(cmd)
                .timeout(7, TimeUnit.SECONDS)
                .observeOn(AndroidSchedulers.mainThread()).subscribe({
                    Toast.makeText(this, getString(R.string.command_term) + " $cmd " + getString(R.string.sent_term), Toast.LENGTH_SHORT).show()
                    dialog.dismiss()
                }, { e ->
                    dialog.dismiss()
                    if (e is BLEException) {
                        finish()
                    } else {
                        Toast.makeText(this, getString(R.string.error_occured), Toast.LENGTH_SHORT).show()
                    }
                })
    }

    @SuppressLint("MissingSuperCall")
    override fun onDestroy() {
        super.onDestroy()
        service.disconnect()

    }

    override fun onStart() {
        super.onStart()
        service.connection.observeOn(AndroidSchedulers.mainThread()).subscribe {
            textViewConnection.text = if (it) getString(R.string.connected_term) else getString(R.string.disconnected_term)
        }.disposedBy(disposeBag)

        //placeService.managedPlaces.subscribe().disposedBy(disposeBag)
    }

    override fun onStop() {
        super.onStop()
        disposeBag.clear()
    }
}
