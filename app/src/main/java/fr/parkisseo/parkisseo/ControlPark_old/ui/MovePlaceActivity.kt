package fr.parkisseo.parkisseo.ControlPark_old.ui

import android.annotation.SuppressLint
import android.os.Bundle
import android.support.v7.app.AlertDialog
import android.view.Menu
import android.view.MenuItem
import com.github.salomonbrys.kodein.android.KodeinAppCompatActivity
import com.github.salomonbrys.kodein.instance

import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.LatLng
import fr.parkisseo.parkisseo.R
import fr.parkisseo.parkisseo.ControlPark_old.services.*
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import kotlinx.android.synthetic.main.activity_move_place.*
import java.util.concurrent.TimeUnit

class MovePlaceActivity : KodeinAppCompatActivity() {

    val placeService: PlaceService by instance()
    val disposeBag = CompositeDisposable()
    var location: LatLng? = null

    @SuppressLint("MissingSuperCall", "MissingPermission")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_move_place)

        val place = intent.getSerializableExtra("place") as Place
        getMap().subscribe { map ->
            map.mapType = GoogleMap.MAP_TYPE_HYBRID
            map.isMyLocationEnabled = true
            map.uiSettings.isMyLocationButtonEnabled = true

            map.animateCamera(CameraUpdateFactory.newLatLngZoom(LatLng(place.latitude, place.longitude), 20f))
//            map.userLocation().take(1).subscribe {
//                map.animateCamera(CameraUpdateFactory.newLatLngZoom(it, 17f))
//            }
        }

        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        title = intent.getStringExtra("title")

    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.move_place_menu, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem) = when (item.itemId) {
        android.R.id.home -> {
            finish()
            true
        }
        R.id.actionMovePlace -> {
            movePlace()
            true
        }
        else -> false
    }

    override fun onResume() {
        super.onResume()
        getMap().flatMapObservable { it.mapCenter() }.throttleLast(100, TimeUnit.MILLISECONDS)
                .observeOn(AndroidSchedulers.mainThread()).subscribe {
            location = it
            textViewStatus.text = getString(R.string.new_position) + " %.4f %.4f".format(it.latitude, it.longitude)
        }.disposedBy(disposeBag)
    }

    override fun onPause() {
        super.onPause()
        disposeBag.clear()
    }

    private fun getMap(): Single<GoogleMap> {
        val mapFragment = supportFragmentManager.findFragmentById(R.id.map) as SupportMapFragment
        return mapFragment.getMap()
    }


    fun movePlace() {
        val location = this.location
        val place = intent.getSerializableExtra("place") as Place
        if (location != null && place.sigfoxId != null) {
            AlertDialog.Builder(this)
                    .setTitle(getString(R.string.reposition_question) + " ${place.name} ?")
                    .setNegativeButton(getString(R.string.no_key)) { _, _ -> }
                    .setPositiveButton(getString(R.string.yes_key)) { _, _ ->
                        placeService
                                .updatePlaceLocation(place.sigfoxId, location.latitude, location.longitude)
                                .subscribeWithProgress(this, message = getString(R.string.repositioning_term),
                                        successMessage = getString(R.string.place_repositioned))
                    }.show()
        }

    }
}

