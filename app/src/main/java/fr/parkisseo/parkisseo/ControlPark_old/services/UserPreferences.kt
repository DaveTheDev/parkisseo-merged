package fr.parkisseo.parkisseo.ControlPark_old.services

import android.content.SharedPreferences
import com.google.android.gms.maps.GoogleMap
import fr.parkisseo.parkisseo.ControlPark_old.ui.PlaceListViewModel.SortType

/**
 * Created by Alexandre Delattre on 05/06/2017.
 */

class UserPreferences(val prefs: SharedPreferences) {
    var sortType: SortType
        get() = when (prefs.getString("sortType", null)) {
            "nearest" -> SortType.nearest
            "mostAbusive" -> SortType.mostAbusive
            else -> SortType.nearest
        }
        set(newValue) = prefs.edit().putString("sortType", when(newValue) {
            SortType.nearest -> "nearest"
            SortType.mostAbusive -> "mostAbusive"
        }).apply()

    var mapType: Int
        get() = prefs.getInt("mapType", GoogleMap.MAP_TYPE_HYBRID)
        set(value) = prefs.edit().putInt("mapType", value).apply()
}