package fr.parkisseo.parkisseo.ControlPark_old.ui

import android.Manifest
import android.annotation.SuppressLint
import android.content.*
import android.content.pm.PackageManager
import android.os.Bundle
import android.support.design.widget.Snackbar
import android.support.design.widget.TabLayout
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentPagerAdapter
import android.support.v4.view.ViewPager
import android.support.v7.app.AlertDialog
import android.util.Log
import android.view.Menu
import android.view.View
import com.google.android.gms.maps.GoogleMap
import fr.parkisseo.parkisseo.ControlPark_old.services.AppCompatActivityExts.kt.isPermissionGranted
import fr.parkisseo.parkisseo.ControlPark_old.services.AppCompatActivityExts.kt.requestPermission
import fr.parkisseo.parkisseo.ControlPark_old.services.AppCompatActivityExts.kt.shouldShowPermissionRationale
import fr.parkisseo.parkisseo.ControlPark_old.services.CollectionsExts.kt.containsOnly
import fr.parkisseo.parkisseo.R
import io.reactivex.disposables.CompositeDisposable
import android.location.LocationManager
import android.graphics.Color
import android.preference.PreferenceManager
import android.provider.Settings
import android.support.v7.app.AppCompatActivity
import android.util.TypedValue
import android.webkit.WebView
import android.webkit.WebViewClient
import android.widget.*
import com.github.salomonbrys.kodein.android.KodeinAppCompatActivity
import com.github.salomonbrys.kodein.instance
import fr.parkisseo.parkisseo.ControlPark_old.services.*
//import com.parkisseo.controlpark.ui.PreferenceHelper.defaultPrefs
import fr.parkisseo.parkisseo.ControlPark_old.services.LoginService
import fr.parkisseo.parkisseo.ControlPark_old.services.PlaceService
import fr.parkisseo.parkisseo.MainActivity
import kotlinx.android.synthetic.main.activity_main_controlpark.*
import kotlin.system.exitProcess


class ControlParkMainActivity : KodeinAppCompatActivity() {
    val disposeBag = CompositeDisposable()
    val  placeService: PlaceService by instance()
    val placeListViewModel: PlaceListViewModel by instance()
    val placeMapViewModel: PlacesMapViewModel by instance()
    val loginService: LoginService by instance()
    var context: Context? = null
    lateinit var term:TermsPref
    private var lm : LocationManager? = null
    //val term:TermsPref? = null
    //var GpsStatus: Boolean = false
    var fragments:TabFragments?=null
    @SuppressLint("MissingSuperCall")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main_controlpark)
        context = getApplicationContext();

       term = TermsPref(this);
       // globalTerm=term
        val term_state=term.getisFirstTime()
        if(term_state){
            privacyAccept()
        }


        val refresh_btn = findViewById(R.id.refresh_btn) as ImageButton
    refresh_btn.setOnClickListener{
       // this.onRestart()
    this.onStop()
    this.onResume()

//        placeService.loading.subscribe { isLoading ->
//            progressBar?.visibility = if (isLoading) View.VISIBLE else View.GONE
//            refresh_btn?.visibility=if(isLoading) View.GONE else View.VISIBLE
//        }.disposedBy(disposeBag)
//        placeService.managedPlaces.subscribe().disposedBy(disposeBag)

}



// Get Location Manager and check for GPS & Network location services
 lm  = getSystemService(LOCATION_SERVICE) as LocationManager?;
if(!lm!!.isProviderEnabled(LocationManager.GPS_PROVIDER) ||
      !lm!!.isProviderEnabled(LocationManager.NETWORK_PROVIDER)) {
  // Build the alert dialog

    val dialogBuilder = AlertDialog.Builder(this)

    dialogBuilder.setTitle(getString(R.string.location_services_notActive))
    dialogBuilder.setCancelable(false)
    dialogBuilder.setMessage(getString(R.string.pleaseEnable_location_GPS))
    dialogBuilder.setPositiveButton("Ok", DialogInterface.OnClickListener { dialog, whichButton ->

        val intent =  Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
        startActivity(intent);

    })
//    dialogBuilder.setNegativeButton("Cancel", DialogInterface.OnClickListener { dialog, whichButton ->
//        //pass
//    })
    val b = dialogBuilder.create()
    b.show()


}

        if (!isPermissionGranted(Manifest.permission.ACCESS_FINE_LOCATION)) {
            requestLocationPermission()

        }else {
             fragments = TabFragments(listOf(
                    TabFragment(getString(R.string.Supervised_places), R.drawable.ic_action_format_list_bulleted) { PlaceListFragment() },
                    TabFragment(getString(R.string.map),               R.drawable.ic_action_location) { PlaceMapFragment() },
                    TabFragment(getString(R.string.settings),           R.drawable.ic_action_settings) { SettingsFragment() }
            ))
            fragments!!.setup(supportFragmentManager, tabLayout, viewPager)
            val listener = object : ViewPager.SimpleOnPageChangeListener() {
                override fun onPageSelected(position: Int) {
                    val title = viewPager.adapter?.getPageTitle(position).toString()
                    toolbar.title = title
                }
            }
            viewPager.addOnPageChangeListener(listener)
            listener.onPageSelected(0)
        }
        toolbar.inflateMenu(R.menu.main_menu_controlpark)
        toolbar.menu.findItem(when(placeListViewModel.sortType.value) {
            PlaceListViewModel.SortType.nearest ->  R.id.itemSortNearest
            PlaceListViewModel.SortType.mostAbusive -> R.id.itemSortAbusive
            null -> R.id.itemSortNearest
        }).isChecked = true


        toolbar.menu.findItem(when (placeMapViewModel.mapType.value) {
            GoogleMap.MAP_TYPE_NORMAL -> R.id.itemMapTypeNormal
            GoogleMap.MAP_TYPE_HYBRID -> R.id.itemMapTypeHybrid
            else -> R.id.itemMapTypeHybrid
        }).isChecked = true


        toolbar.setOnMenuItemClickListener {
            when (it.itemId) {
                R.id.itemSortNearest -> {
                    it.isChecked = true
                    placeListViewModel.sortType.onNext(PlaceListViewModel.SortType.nearest)
                    true
                }
                R.id.itemSortAbusive -> {
                    it.isChecked = true
                    placeListViewModel.sortType.onNext(PlaceListViewModel.SortType.mostAbusive)
                    true
                }
                R.id.itemMapTypeNormal -> {
                    it.isChecked = true
                    placeMapViewModel.mapType.onNext(GoogleMap.MAP_TYPE_NORMAL)
                    true
                }
                R.id.itemMapTypeHybrid -> {
                    it.isChecked = true
                    placeMapViewModel.mapType.onNext(GoogleMap.MAP_TYPE_HYBRID)
                    true
                }
                R.id.item_disconnect -> {
                    disconnect()
                    true
                }
                R.id.privacy -> {
                    privacy()
                    true
                }
                R.id.action_switchToParkiCity -> {
                    finish()
                    true
                }
                else -> false
            }
        }

//        requestLocationPermission()

    }


    override fun onStart() {
        super.onStart()
        //Log.d("onstart","onstart")
//        placeService.loading.subscribe { isLoading ->
//            progressBar?.visibility = if (isLoading) View.VISIBLE else View.GONE
//            refresh_btn?.visibility=if(isLoading) View.GONE else View.VISIBLE
//        }.disposedBy(disposeBag)
//        placeService.managedPlaces.subscribe().disposedBy(disposeBag)


    }


    override fun onStop() {
        super.onStop()
        disposeBag.clear()
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
//        menuInflater.inflate(R.menu.main_menu, menu)
        return true
    }

    private fun disconnect() {
        AlertDialog.Builder(this)
                .setTitle(getString(R.string.ask_disconnect))
                .setNegativeButton(getString(R.string.no_key)) { _, _ -> }
//                .setPositiveButton("Oui") { _, _ ->
//                    loginService.logout().subscribeWithProgress(this, "Déconnexion") {
//                        finish()
//                        startActivity<LoginActivity>()
//                    }
                .setPositiveButton(getString(R.string.yes_key)) { _, _ ->
                    logout()
                }.show()
    }
fun logout(){
    //term.setisFirstTime(state)
    //AppPreferences.firstRun=true
    val app_preferences = PreferenceManager.getDefaultSharedPreferences(this)
    val editor = app_preferences.edit()
    editor.putBoolean("rememberMe", false)
    editor.apply()
    finish()
    startActivity<MainActivity>()
//    setFirstTime(true)
//    loginService.logout().subscribeWithProgress(this, getString(R.string.sign_out)) {
//
//    }
}

    private fun privacy() {
        val alert = AlertDialog.Builder(this)
        val wv = WebView(this)

        alert.setTitle(getString(R.string.privacy_policy))
        alert.setCancelable(false)
        alert.setNegativeButton(R.string.close_key) { dialog, id -> dialog.dismiss() }
        alert.setView(wv)

        wv.settings.javaScriptEnabled=true
        wv.isVerticalScrollBarEnabled=true
        wv.loadUrl("https://sites.google.com/view/abedtest/home?authuser=1")
        wv.setWebViewClient(object : WebViewClient() {
            override fun shouldOverrideUrlLoading(view: WebView, url: String): Boolean {
                view.loadUrl(url)
                return true
            }

            })
            alert.show()

        }

    fun privacyAccept() {
        val alert = AlertDialog.Builder(this)
        val wv = WebView(this)
        val cbx = CheckBox(this)
        val terms=TextView(this)
        terms.setText("\t\t\t" + getString(R.string.terms_conditions));
        terms.setTextSize(TypedValue.COMPLEX_UNIT_SP,10f)
        alert.setTitle(getString(R.string.privacy_policy))
        alert.setCancelable(false)
        val dialogClick=DialogInterface.OnClickListener{_,which->
            when(which){
           DialogInterface.BUTTON_POSITIVE -> setFirstTime(false)
                DialogInterface.BUTTON_NEGATIVE ->   exitApp()

            }
        }
        alert.setNegativeButton(getString(R.string.decline_key),dialogClick)
       // alert.setPositiveButton("Accept"){ dialog, id -> dialog.dismiss()}
        alert.setPositiveButton(getString(R.string.accept_key),dialogClick)


        wv.loadUrl("https://sites.google.com/view/abedtest/home?authuser=1")
        wv.setWebViewClient(object : WebViewClient() {
            override fun shouldOverrideUrlLoading(view: WebView, url: String): Boolean {
                view.loadUrl(url)
                return true
            }

        })
        wv.visibility=View.GONE
        var positive=Button(this)

        terms.setTextColor(Color.BLUE)

        terms.setOnClickListener{
            if(wv.isShown){
                wv.visibility = View.GONE
            }else {
                wv.visibility = View.VISIBLE
            }
        }
        cbx.setText(getString(R.string.accepted_terms))
        cbx.setOnCheckedChangeListener{ buttonView, isChecked ->
            if(isChecked){
                positive.isEnabled=true

            }else{
                positive.isEnabled=false
            }
        }
        wv.settings.javaScriptEnabled=true
        wv.isVerticalScrollBarEnabled=true


        val layout=LinearLayout(this)
        layout.orientation=LinearLayout.VERTICAL
        layout.isVerticalScrollBarEnabled=true
        layout.addView(cbx)
        layout.addView(terms)
        layout.addView(wv)

        val dialog=alert.create();
        dialog.setView(layout)
        dialog.show()
        positive=dialog.getButton(AlertDialog.BUTTON_POSITIVE)
        positive.isEnabled=false


    }
    fun Context.toast(message: String) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show()
    }
    fun exitApp()
    {
        moveTaskToBack(true)
        exitProcess(-1)
    }
    fun setFirstTime(state:Boolean){
        term.setisFirstTime(state)
    }
//    private fun requestLocationPermission() {
//        //TODO improve workflow in case user reject the permission the first time
//        if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
//            if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.ACCESS_FINE_LOCATION)) {
//                // Show an expanation to the user *asynchronously* -- don't block
//                // this thread waiting for the user's response! After the user
//                // sees the explanation, try again to request the permission.
//
//            } else {
//                ActivityCompat.requestPermissions(this, arrayOf(Manifest.permission.ACCESS_FINE_LOCATION), 1234);
//            }
//        }
//    }
     private fun requestLocationPermission() {
        Log.i("Permission", "Location permission has NOT been granted. Requesting permission.")
        if (shouldShowPermissionRationale(Manifest.permission.ACCESS_FINE_LOCATION)) {
            // Provide an additional rationale to the user if the permission was not granted
            // and the user would benefit from additional context for the use of the permission.
            // For example if the user has previously denied the permission.
            Log.i("Permission", "Displaying Location permission rationale to provide additional context.")
           requestPermission(Manifest.permission.ACCESS_FINE_LOCATION, LOCATION_REQUEST)

        } else {

            // location permission has not been granted yet. Request it directly.
            requestPermission(Manifest.permission.ACCESS_FINE_LOCATION, LOCATION_REQUEST)
        }
    }
    companion object {

        const val TAG = "MainActivity2"

        const val LOCATION_REQUEST = 2
        /**
         * Id to identify a location permission request.
         */
        val PERMISSIONS_LOCATION = arrayOf(Manifest.permission.ACCESS_FINE_LOCATION)
    }
    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String>,
                                            grantResults: IntArray) {

        if (requestCode == LOCATION_REQUEST) {
            // Received permission result for Location permission.
            Log.i(TAG, "Received response for Location permission request.")

            // Check if the permission has been granted
            if (grantResults.containsOnly(PackageManager.PERMISSION_GRANTED)) {
                // location permission has been granted, preview can be displayed
                Log.i(TAG, "Location permission has now been granted. Showing Map.")
                Snackbar.make(coordinate, getString(R.string.location_services_permitted),
                        Snackbar.LENGTH_SHORT).show()
                 fragments = TabFragments(listOf(
                        TabFragment(getString(R.string.Supervised_places), R.drawable.ic_action_format_list_bulleted) { PlaceListFragment() },
                        TabFragment(getString(R.string.map)             ,R.drawable.ic_action_location) { PlaceMapFragment() },
                        TabFragment(getString(R.string.settings)        ,R.drawable.ic_action_settings) { SettingsFragment() }
                ))
                fragments!!.setup(supportFragmentManager, tabLayout, viewPager)
                val listener = object : ViewPager.SimpleOnPageChangeListener() {
                    override fun onPageSelected(position: Int) {
                        val title = viewPager.adapter?.getPageTitle(position).toString()
                        toolbar.title = title
                    }
                }
                viewPager.addOnPageChangeListener(listener)
                listener.onPageSelected(0)
            } else {
                Log.i(TAG, "Location permission was NOT granted.")
                Snackbar.make(coordinate,getString(R.string.permissions_not_permitted),
                        Snackbar.LENGTH_SHORT).show()

            }
        } else {
            super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        }
    }


}



class TabFragments(private val tabs: List<TabFragment>) {

    fun setup(fragmentManager: FragmentManager, tabLayout: TabLayout, viewPager: ViewPager) {
        for (tab in tabs) {
            tabLayout.addTab(tabLayout.newTab().setIcon(tab.iconRes))
        }

        viewPager.adapter = object : FragmentPagerAdapter(fragmentManager) {
            override fun getItem(position: Int): Fragment {
                return tabs[position].fragmentProvider()
            }

            override fun getCount(): Int {
                return tabs.size
            }

            override fun getPageTitle(position: Int): CharSequence {
                return tabs[position].title
            }
        }
        viewPager.addOnPageChangeListener(TabLayout.TabLayoutOnPageChangeListener(tabLayout))
        tabLayout.setOnTabSelectedListener(TabLayout.ViewPagerOnTabSelectedListener(viewPager))
    }
}

class TabFragment(val title: String, val iconRes: Int, val fragmentProvider: () -> Fragment)


