package fr.parkisseo.parkisseo.ControlPark_old.ui

import android.annotation.SuppressLint
import android.content.Intent
import android.graphics.*
import android.graphics.drawable.Drawable
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.github.salomonbrys.kodein.android.KodeinSupportFragment
import com.github.salomonbrys.kodein.instance
import fr.parkisseo.parkisseo.R
import fr.parkisseo.parkisseo.ControlPark_old.services.*
import io.reactivex.Observable
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.functions.BiFunction
import io.reactivex.subjects.BehaviorSubject
import kotlinx.android.synthetic.main.place_item.view.*
import kotlinx.android.synthetic.main.places_list.view.*

class PlaceListFragment : KodeinSupportFragment() {
    val viewModel: PlaceListViewModel by instance()
    val disposeBag = CompositeDisposable()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val v = inflater.inflate(R.layout.places_list, container, false)
        v.recyclerView.layoutManager = LinearLayoutManager(context)
        return v
    }

    override fun onResume() {
        super.onResume()
        viewModel.places.subscribe { places ->
            view?.recyclerView?.swapAdapter(PlaceListAdapter(places), false)
        }.disposedBy(disposeBag)

    }

    override fun onPause() {
        super.onPause()
        disposeBag.clear()
    }
}


class PlaceListViewModel(placeService: PlaceService, userPreferences: UserPreferences) {
    enum class SortType {
        nearest, mostAbusive
    }

    val places: Observable<List<Pair<Place, Float>>>
    val sortType = BehaviorSubject.createDefault(userPreferences.sortType)

    init {
        places = Observable.combineLatest(placeService.managedPlaces, sortType, BiFunction { p, s ->
            when (s) {
                PlaceListViewModel.SortType.nearest ->
                    p.sortedWith(compareBy(
                            {it.first.stateScore()},
                            {it.second}
                    ))
                PlaceListViewModel.SortType.mostAbusive ->
                    p.sortedWith(compareBy(
                            {it.first.stateScore()},
                            {it.first.lastEventTime()}
                    ))
            }
        })

        sortType.subscribe { userPreferences.sortType = it }
    }
}

fun Place.lastEventTime(): Long = lastEventDate?.time ?: 0

class PlaceViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
    val imageView = itemView.place_state
    val nameTextView = itemView.textViewName
    val infoTextView = itemView.textViewInfo

    fun display(placeAndDist: Pair<Place, Float>) {
        val (place, dist) = placeAndDist
        imageView.setImageDrawable(PlaceDrawable(place))
        nameTextView.text = place.name

        val dateString = place.lastEventDate?.let { formatTimeAgo(it) } ?: ""
        infoTextView.text = "${formatDist(dist.toDouble())}\n$dateString"

        itemView.setOnClickListener {
            itemView.context.startActivity(Intent(itemView.context, PlaceDetailActivity::class.java)
                    .putExtra("place", place))
        }
    }
}

class PlaceListAdapter(val places: List<Pair<Place, Float>>) : RecyclerView.Adapter<PlaceViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PlaceViewHolder {
        val v = LayoutInflater.from(parent.context).inflate(R.layout.place_item, parent, false)
        return PlaceViewHolder(v)
    }

    override fun onBindViewHolder(holder: PlaceViewHolder, position: Int) {
        holder.display(places[position])
    }

    override fun getItemCount(): Int {
        return places.size
    }

}

private val placePaint = Paint().apply {
    style = Paint.Style.FILL
    color = Color.argb(255, 108, 114, 122)
    isAntiAlias = true
}
class PlaceDrawable(val place: Place): Drawable() {
    override fun draw(c: Canvas) {
        val inset = bounds.height() * 0.1f
        val rect = RectF(bounds)
        rect.inset(inset, inset)

        placePaint.color = place.toColor()
        c.drawCircle(rect.centerX(), rect.centerY(), rect.width() / 2, placePaint)
    }

    override fun setAlpha(p0: Int) {
    }

    @SuppressLint("WrongConstant")
    override fun getOpacity(): Int {
        return 255
    }

    override fun setColorFilter(p0: ColorFilter?) {
    }

}

fun Place.toColor(): Int {
    return when (state) {
        fr.parkisseo.parkisseo.ControlPark_old.services.State.abuse ->  Color.RED
        fr.parkisseo.parkisseo.ControlPark_old.services.State.free -> Color.GREEN
        fr.parkisseo.parkisseo.ControlPark_old.services.State.occupied -> Color.YELLOW
        fr.parkisseo.parkisseo.ControlPark_old.services.State.maintenance -> Color.argb(255, 86, 161, 239)
    }
}