package fr.parkisseo.parkisseo.ControlPark_old.services

import android.bluetooth.BluetoothDevice
import android.bluetooth.BluetoothManager
import android.bluetooth.le.ScanCallback
import android.bluetooth.le.ScanFilter
import android.bluetooth.le.ScanResult
import android.bluetooth.le.ScanSettings
import android.content.Context
import android.os.ParcelUuid
import io.reactivex.Observable
import io.reactivex.functions.BiFunction
import timber.log.Timber
import java.util.*

val PARKISSEO_SERVICE = UUID.fromString("cb120001-ca32-4031-8f91-3546bd912161")

sealed class PlaceResult {
    object Resolving : PlaceResult()
    data class Resolved(val place: Place?): PlaceResult()


    fun getResultPlace() = when(this) {
        Resolving -> null
        is Resolved -> place
    }
}

data class PlaceDevice(val device: BluetoothDevice, val localName: String, val placeResult: PlaceResult)

class ScanBleService(context: Context, placeService: PlaceService) {
    val manager = context.getSystemService(Context.BLUETOOTH_SERVICE) as BluetoothManager


    val devices: Observable<List<PlaceDevice>>

    init {
        val scannedDevices = scanDevices().distinct { it.first.address }
                .scan(emptyList<Pair<BluetoothDevice, String>>()) { devices, dev -> devices + dev }

        val places = placeService.managedPlaces!!.startWith(emptyList<Pair<Place, Float>>())
        devices = Observable.combineLatest(scannedDevices, places, BiFunction { devs, places ->
            if (places.isEmpty()) {
                devs.map { dev -> PlaceDevice(dev.first, dev.second.trimLeadingZeros(), PlaceResult.Resolving) }
            } else {
                devs.map { dev ->
                    val localName = dev.second.trimLeadingZeros()
                    val place = places.find { it.first.sigfoxId == localName }?.first
                    PlaceDevice(dev.first, localName, PlaceResult.Resolved(place))
                }
            }

        })
    }

    private fun scanDevices(): Observable<Pair<BluetoothDevice, String>> {
        return Observable.create<Pair<BluetoothDevice, String>> { s ->
            val callback = object : ScanCallback() {
                override fun onScanResult(callbackType: Int, result: ScanResult) {
                    val deviceName = result.scanRecord.deviceName
//                    Timber.i("onScanResult $callbackType: $deviceName")
                    s.onNext(result.device to deviceName)

                }

                override fun onBatchScanResults(results: MutableList<ScanResult>) {
//                    Timber.i("onBatchScanResults ${results.size}")
                    results.forEach {
                        s.onNext(it.device to it.scanRecord.deviceName)
                    }
                }

                override fun onScanFailed(errorCode: Int) {
                    s.onError(Exception("Cannot start BLE scan, error code: $errorCode"))
                }
            }

            Timber.i("Starting BLE Scan")
            val scanner = manager.adapter.bluetoothLeScanner
            val filter = ScanFilter.Builder().setServiceUuid(ParcelUuid(PARKISSEO_SERVICE)).build()
            val settings = ScanSettings.Builder()
                    .setScanMode(ScanSettings.SCAN_MODE_BALANCED)
                    .build()

            scanner.startScan(listOf(filter), settings, callback)
            s.setCancellable {
                Timber.i("Stopping BLE Scan")
                scanner.stopScan(callback)
            }
        }
    }
}