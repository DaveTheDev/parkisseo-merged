package fr.parkisseo.parkisseo.ControlPark_old.ui

import android.animation.Animator
import android.animation.AnimatorListenerAdapter
import android.annotation.SuppressLint
import android.annotation.TargetApi

import android.os.Build
import android.os.Bundle
import android.support.design.widget.Snackbar
import android.text.TextUtils
import android.view.View
import android.view.inputmethod.EditorInfo
import android.widget.*
import fr.parkisseo.parkisseo.ControlPark_old.services.Credentials
import fr.parkisseo.parkisseo.ControlPark_old.services.UserService
import fr.parkisseo.parkisseo.ControlPark_old.services.startActivity

import com.github.salomonbrys.kodein.android.KodeinAppCompatActivity
import com.github.salomonbrys.kodein.instance
import fr.parkisseo.parkisseo.ControlPark_old.services.LoginService
import fr.parkisseo.parkisseo.ControlPark_old.services.startActivity
import fr.parkisseo.parkisseo.R
import io.reactivex.android.schedulers.AndroidSchedulers
import timber.log.Timber
import android.R.id.edit
import android.content.SharedPreferences
import android.preference.PreferenceManager
import android.util.Base64
import fr.parkisseo.parkisseo.ControlPark_old.ui.ControlParkMainActivity


/**
 * A login screen that offers login via email/password.
 */
class LoginActivity : KodeinAppCompatActivity() {
    // UI references.
    private var mEmailView: AutoCompleteTextView? = null
    private var mPasswordView: EditText? = null
    private var mProgressView: View? = null
    private var mLoginFormView: View? = null
    private var checkbox_rememberMe: CheckBox? = null

    var rememberMe:Boolean = false

    val loginService: LoginService by instance()
    val userService: UserService by instance()


    @SuppressLint("MissingSuperCall")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)
        // Set up the login form.
        mEmailView = findViewById(R.id.email) as AutoCompleteTextView

        mEmailView!!.setOnEditorActionListener { textView, i, keyEvent ->
            mPasswordView?.requestFocus()
            true
        }

        mPasswordView = findViewById(R.id.password) as EditText
        mPasswordView!!.setOnEditorActionListener(TextView.OnEditorActionListener { textView, id, keyEvent ->
            if (id == R.id.login || id == EditorInfo.IME_NULL) {
                attemptLogin()
                return@OnEditorActionListener true
            }
            false
        })

        checkbox_rememberMe = findViewById(R.id.checkbox_rememberMe) as CheckBox
        checkbox_rememberMe!!.setOnCheckedChangeListener { buttonView, isChecked ->
            rememberMe = isChecked
        }




        val mEmailSignInButton = findViewById(R.id.email_sign_in_button) as Button
        mEmailSignInButton.setOnClickListener { attemptLogin() }

        mLoginFormView = findViewById(R.id.login_form)
        mProgressView = findViewById(R.id.login_progress)


        val app_preferences = PreferenceManager.getDefaultSharedPreferences(this)
        val rememberMePrefs = app_preferences.getBoolean("rememberMe", false)

        if (rememberMePrefs) {
            startMainActivity()
            overridePendingTransition(0, 0)
        }
    }

    /**
     * Attempts to sign in or register the account specified by the login form.
     * If there are form errors (invalid email, missing fields, etc.), the
     * errors are presented and no actual login attempt is made.
     */
    private fun attemptLogin() {
        // Reset errors.
        mEmailView!!.error = null
        mPasswordView!!.error = null

        // Store values at the time of the login attempt.
        val email = mEmailView!!.text.toString()
        val password = mPasswordView!!.text.toString()

        var cancel = false
        var focusView: View? = null

        // Check for a valid password, if the user entered one.
        if (!TextUtils.isEmpty(password) && !isPasswordValid(password)) {
            mPasswordView!!.error = getString(R.string.error_invalid_password)
            focusView = mPasswordView
            cancel = true
        }

        // Check for a valid email address.
        if (TextUtils.isEmpty(email)) {
            mEmailView!!.error = getString(R.string.error_field_required)
            focusView = mEmailView
            cancel = true
        } else if (!isEmailValid(email)) {
            mEmailView!!.error = getString(R.string.error_invalid_email)
            focusView = mEmailView
            cancel = true
        }

        if (cancel) {
            // There was an error; don't attempt login and focus the first
            // form field with an error.
            focusView!!.requestFocus()
        } else {
            showProgress(true)
            val credentials = email + ":" + password
            Timber.wtf("creden: " + credentials);

//            Thread({
//                val response = khttp.get(
//                        url = "https://dev-api.parkisseo.com/users/self",
//                        headers = mapOf("accept" to "application/json", "Authorization" to " Basic " + Base64.encodeToString(credentials.toByteArray(), Base64.NO_WRAP))
//                )
//                runOnUiThread({
//                    if(response.statusCode == 200) {
//
//                        val app_preferences = PreferenceManager.getDefaultSharedPreferences(this)
//                        val editor = app_preferences.edit()
//                        editor.putBoolean("rememberMe", rememberMe)
//                        editor.apply()
//
//                        userService.credentials = Credentials(email,password);
//                        startMainActivity()
//                    }
//                    else if (response.statusCode == 500 || response.statusCode == 400)
//                    {
//                        showProgress(false)
//                        Snackbar.make(mEmailView!!, getString(R.string.error_occured), Snackbar.LENGTH_SHORT).show()
//                    }
//                    else
//                    {
//                        showProgress(false)
//                        Snackbar.make(mEmailView!!, getString(R.string.invalid_credentials), Snackbar.LENGTH_SHORT).show()
//                    }
//                })
//            }).start();

            loginService.login(Credentials(email, password))
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe({ valid ->
                        Timber.d("login valid $valid")
                        if (!valid) {
                            Snackbar.make(mEmailView!!, getString(R.string.invalid_credentials), Snackbar.LENGTH_SHORT).show()
                        } else {

                            val app_preferences = PreferenceManager.getDefaultSharedPreferences(this)
                            val editor = app_preferences.edit()
                            editor.putBoolean("rememberMe", rememberMe)
                            editor.apply()

                            startMainActivity()
                        }
                        showProgress(false)
                    }, { e ->
                        showProgress(false)
                        Snackbar.make(mEmailView!!, getString(R.string.error_occured), Snackbar.LENGTH_SHORT).show()
                    })
        }
    }





    private fun isEmailValid(email: String): Boolean {
        return true
    }

    private fun isPasswordValid(password: String): Boolean {
        return true
    }

    /**
     * Shows the progress UI and hides the login form.
     */
    @TargetApi(Build.VERSION_CODES.HONEYCOMB_MR2)
    private fun showProgress(show: Boolean) {
        // On Honeycomb MR2 we have the ViewPropertyAnimator APIs, which allow
        // for very easy animations. If available, use these APIs to fade-in
        // the progress spinner.
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR2) {
            val shortAnimTime = resources.getInteger(android.R.integer.config_shortAnimTime)

            mLoginFormView!!.visibility = if (show) View.GONE else View.VISIBLE
            mLoginFormView!!.animate().setDuration(shortAnimTime.toLong()).alpha(
                    (if (show) 0 else 1).toFloat()).setListener(object : AnimatorListenerAdapter() {
                override fun onAnimationEnd(animation: Animator) {
                    mLoginFormView!!.visibility = if (show) View.GONE else View.VISIBLE
                }
            })

            mProgressView!!.visibility = if (show) View.VISIBLE else View.GONE
            mProgressView!!.animate().setDuration(shortAnimTime.toLong()).alpha(
                    (if (show) 1 else 0).toFloat()).setListener(object : AnimatorListenerAdapter() {
                override fun onAnimationEnd(animation: Animator) {
                    mProgressView!!.visibility = if (show) View.VISIBLE else View.GONE
                }
            })
        } else {
            // The ViewPropertyAnimator APIs are not available, so simply show
            // and hide the relevant UI components.
            mProgressView!!.visibility = if (show) View.VISIBLE else View.GONE
            mLoginFormView!!.visibility = if (show) View.GONE else View.VISIBLE
        }
    }

    private fun startMainActivity() {
        startActivity<ControlParkMainActivity>()
        finish()
    }
}

