package fr.parkisseo.parkisseo.ControlPark_old.services

import android.annotation.SuppressLint
import android.location.Location
import com.google.android.gms.location.LocationRequest
import com.patloew.rxlocation.RxLocation
import io.reactivex.Observable
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.subjects.BehaviorSubject
import okhttp3.ResponseBody
import retrofit2.http.Body
import retrofit2.http.GET
import retrofit2.http.POST
import timber.log.Timber
import java.util.concurrent.TimeUnit

data class UpdatePlaceLocation(val sigfoxId: String, val lat: Double, val lon: Double)

interface PlaceWebService {

    @GET("/managedPlaces")
    fun managedPlaces() : Single<List<Place>>

    @POST("/updatePlaceLocation")
    fun updatePlaceLocation(@Body body: UpdatePlaceLocation): Single<ResponseBody>
}

class PlaceService (val ws: PlaceWebService, val rxLocation: RxLocation) {



    var managedPlaces: Observable<List<Pair<Place, Float>>>
    val loading: Observable<Boolean>

    init {
            loading = BehaviorSubject.create()
            managedPlaces = Observable.interval(30, TimeUnit.SECONDS).startWith(0)
                    .observeOn(AndroidSchedulers.mainThread())
                    .doOnNext {
                        Timber.i("Refreshing placessss")
                        loading.onNext(true)
                    }
                    .switchMap {
                        getLocation().logOperation("Getting location").retryWhen { err -> err.delay(3, TimeUnit.SECONDS) }.flatMap {
                            loc ->
                                Timber.i("Got location $loc")
                                ws.managedPlaces()
                                        .logOperation("Fetching places")
                                        .retryWhen { err -> err.delay(3, TimeUnit.SECONDS) }
                                        .map { it.map { it to computeDistance(loc, it) } }
                            }.toObservable()


                    }
                    .observeOn(AndroidSchedulers.mainThread())
                    .doOnNext {
                        loading.onNext(false)
                    }
                    .doOnSubscribe { Timber.i("Subscribed to managed places updates") }
                    .doOnDispose { Timber.i("Unsubscribed to managed places updates") }
                    .replay(1).refCount()

    }


    @SuppressLint("MissingPermission")
     fun getLocation(): Single<Location> {

            val locationRequest = LocationRequest.create()
                    .setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY)
                    .setNumUpdates(1)

        return rxLocation.location().updates(locationRequest).take(1).singleOrError()
    }


     fun computeDistance(loc: Location, place: Place): Float {
        val placeLoc = Location("FAKE")
        placeLoc.latitude = place.latitude
        placeLoc.longitude = place.longitude
        return loc.distanceTo(placeLoc)
    }

    fun updatePlaceLocation(sigfoxId: String, lat: Double, lon: Double) =
            ws.updatePlaceLocation(UpdatePlaceLocation(sigfoxId, lat, lon))
                    .retry(3)


}
