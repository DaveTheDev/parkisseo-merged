//package fr.parkisseo.parkisseo.ControlPark_old.services
//
//import android.app.NotificationManager
//import android.app.PendingIntent
//import android.content.Context
//import android.content.Intent
//import android.media.RingtoneManager
//import android.os.Bundle
//import android.support.v4.app.NotificationCompat
//import com.beincognito.beincognito.services.PushRegistrationService
//import com.github.salomonbrys.kodein.KodeinInjector
//import com.github.salomonbrys.kodein.android.ServiceInjector
//import com.github.salomonbrys.kodein.instance
//import com.google.android.gms.gcm.GcmListenerService
//import com.google.android.gms.iid.InstanceIDListenerService
//import fr.parkisseo.parkisseo.R
//import fr.parkisseo.parkisseo.ControlPark_old.ui.ControlParkMainActivity
//import timber.log.Timber
//
///**
// * Created by Alexandre Delattre on 30/05/2017.
// */
//class MyInstanceIdService: InstanceIDListenerService(), ServiceInjector {
//    override val injector: KodeinInjector = KodeinInjector()
//
//    val pushRegistrationService: PushRegistrationService by injector.instance()
//
//    override fun onCreate() {
//        super.onCreate()
//        initializeInjector()
//    }
//
//    override fun onDestroy() {
//        super.onDestroy()
//        destroyInjector()
//    }
//
//    override fun onTokenRefresh() {
//        super.onTokenRefresh()
//        Timber.i("onTokenRefresh called")
//        pushRegistrationService.updateCurrentToken()
//    }
//}
//
//class MyGcmListenerService : GcmListenerService() {
//
//    override fun onMessageReceived(from: String, data: Bundle) {
//        Timber.i("Got notification $data")
//        data.getBundle("notification")?.apply {
//            val title = getString("title") ?: "Parkisseo"
//            val body = getString("body") ?: ""
//            sendNotification(title, body)
//        }
//    }
//
//    private fun sendNotification(title: String, messageBody: String) {
//        val intent = Intent(this, ControlParkMainActivity::class.java)
//        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
//        val pendingIntent = PendingIntent.getActivity(this, 0 /* Request code */, intent,
//                PendingIntent.FLAG_ONE_SHOT)
//
//        val defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION)
//        val notificationBuilder = NotificationCompat.Builder(this)
//                .setSmallIcon(R.mipmap.ic_launcher)
//                .setContentTitle(title)
//                .setContentText(messageBody)
//                .setAutoCancel(true)
//                .setSound(defaultSoundUri)
//                .setContentIntent(pendingIntent)
//
//        val notificationManager = getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
//
//        notificationManager.notify(0 /* ID of notification */, notificationBuilder.build())
//    }
//}