package fr.parkisseo.parkisseo.ControlPark_old.ui

import android.annotation.SuppressLint
import android.content.Intent
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.MenuItem
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import fr.parkisseo.parkisseo.ControlPark_old.services.disposedBy
import com.github.salomonbrys.kodein.android.KodeinAppCompatActivity
import com.github.salomonbrys.kodein.instance
import fr.parkisseo.parkisseo.R
import fr.parkisseo.parkisseo.ControlPark_old.services.PlaceDevice
import fr.parkisseo.parkisseo.ControlPark_old.services.PlaceResult
import fr.parkisseo.parkisseo.ControlPark_old.services.ScanBleService
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import kotlinx.android.synthetic.main.activity_place_blelist.*
import kotlinx.android.synthetic.main.ble_item.view.*
import timber.log.Timber
import java.util.concurrent.TimeUnit

class PlaceBLEListActivity : KodeinAppCompatActivity() {

    val disposeBag = CompositeDisposable()
    val scanBleService: ScanBleService by instance()

    @SuppressLint("MissingSuperCall")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_place_blelist)

        recyclerView.layoutManager = LinearLayoutManager(this)

        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        progressBar.visibility = View.VISIBLE
        scanBleService.devices
                .takeUntil(Observable.timer(60, TimeUnit.SECONDS))
                .throttleLast(500, TimeUnit.MILLISECONDS)
                .map { devs -> devs.sortedWith(
                        compareBy({it.placeResult.getResultPlace()?.name ?: "z"}, {it.localName})
                )}
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    recyclerView.swapAdapter(BleListAdapter(it), false)
                }, { err ->
                    Toast.makeText(this, getString(R.string.impossible_start_scan), Toast.LENGTH_SHORT).show()
                    Timber.e(err)
                    progressBar.visibility = View.GONE
                }, {
                    progressBar.visibility = View.GONE
                }).disposedBy(disposeBag)
    }

    override fun onOptionsItemSelected(item: MenuItem) = when (item.itemId) {
        android.R.id.home -> {
            finish()
            true
        }
        else -> false
    }

    @SuppressLint("MissingSuperCall")
    override fun onDestroy() {
        super.onDestroy()
        disposeBag.clear()
    }
}

class BleViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
    val textView = itemView.textViewName
    val progressBar = itemView.progressBarBle


    fun display(device: PlaceDevice) {
        val (dev, localName, result) = device
        when(result) {
            PlaceResult.Resolving -> {
                textView.text = localName
                progressBar.visibility = View.VISIBLE
            }
            is PlaceResult.Resolved -> {
                val name = result.place?.name ?: itemView.context.getString(R.string.place_not_recognized)
                textView.text = "$localName - $name"
                progressBar.visibility = View.GONE
            }
        }

        itemView.setOnClickListener {

            itemView.context.startActivity(Intent(itemView.context, PlaceBLEDetailActivity::class.java)
                    .putExtra("device", dev)
                    .putExtra("title", textView.text.toString())
                    .putExtra("place", result.getResultPlace())
            )
        }
    }
}
class BleListAdapter(val devices: List<PlaceDevice>) : RecyclerView.Adapter<BleViewHolder>() {
    override fun getItemCount() = devices.size

    override fun onBindViewHolder(holder: BleViewHolder, position: Int) = holder.display(devices[position])

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BleViewHolder =
            BleViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.ble_item, parent, false))

}