package fr.parkisseo.parkisseo.ControlPark_old.services

import java.io.Serializable
import java.util.*

data class Place(
        val id: Int,
        val name: String,
        val state: State,
        val latitude: Double,
        val longitude: Double,
        val lastEventDate: Date?,
        val sigfoxId: String?

) : Serializable {

    fun stateScore() = when(state) {
        State.abuse -> 0
        State.occupied -> 1
        State.free -> 2
        State.maintenance -> 3
    }
}

enum class State {
    free, occupied, abuse, maintenance
}