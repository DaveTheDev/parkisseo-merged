package fr.parkisseo.parkisseo.ControlPark_old.ui

import android.annotation.SuppressLint
import android.os.Bundle
import android.view.MenuItem
import com.github.salomonbrys.kodein.android.KodeinAppCompatActivity

import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions
import fr.parkisseo.parkisseo.ControlPark_old.services.Place
import fr.parkisseo.parkisseo.R

class PlaceDetailActivity : KodeinAppCompatActivity(), OnMapReadyCallback {

    private var mMap: GoogleMap? = null

    @SuppressLint("MissingSuperCall")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_place_detail)
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        val mapFragment = supportFragmentManager
                .findFragmentById(R.id.map) as SupportMapFragment
        mapFragment.getMapAsync(this)

        val place = intent.getSerializableExtra("place") as Place
        title = place.name

        supportActionBar?.setDisplayHomeAsUpEnabled(true)
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        finish()
        return true
    }


    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    override fun onMapReady(googleMap: GoogleMap) {
        mMap = googleMap
        googleMap.mapType = GoogleMap.MAP_TYPE_HYBRID

        val place = intent.getSerializableExtra("place") as Place
        // Add a marker in Sydney and move the camera
        val marker = LatLng(place.latitude, place.longitude)
        mMap!!.addMarker(MarkerOptions()
                .position(marker)
                .icon(getMarkerIcon(place))
                .title(place.name)
                .snippet(place.stateString())
        )
        mMap!!.moveCamera(CameraUpdateFactory.newLatLngZoom(marker, 18f))
    }
}
