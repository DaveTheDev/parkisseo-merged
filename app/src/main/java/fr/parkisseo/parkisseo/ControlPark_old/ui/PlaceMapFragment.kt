package fr.parkisseo.parkisseo.ControlPark_old.ui

import android.annotation.SuppressLint
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.github.salomonbrys.kodein.android.KodeinSupportFragment
import com.github.salomonbrys.kodein.instance
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.BitmapDescriptorFactory
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions
import fr.parkisseo.parkisseo.ControlPark_old.services.*
import fr.parkisseo.parkisseo.R

import io.reactivex.Single
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.subjects.BehaviorSubject

class PlaceMapFragment : KodeinSupportFragment()  {


    val viewModel: PlacesMapViewModel by instance()
    val disposeBag = CompositeDisposable()

    @SuppressLint("MissingPermission")
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val v:View = inflater!!.inflate(R.layout.places_map, container, false)

getMap().subscribe { map ->
            map.isMyLocationEnabled = true
            map.uiSettings.isMyLocationButtonEnabled = true
            map.mapType = GoogleMap.MAP_TYPE_HYBRID
            map.moveCamera(CameraUpdateFactory.newLatLngZoom(LatLng(46.60416716, 2.65869141), 5f))
        }

        return v


    }

    private fun getMap(): Single<GoogleMap> {
        val mapFragment = childFragmentManager.findFragmentById(R.id.map) as SupportMapFragment
        return mapFragment.getMap()
    }

    override fun onResume() {
        super.onResume()

        getMap().flatMapObservable { map ->
            viewModel.places!!.doOnNext {
                updateMarkers(map, it)
            }
        }.subscribe().disposedBy(disposeBag)

        getMap().flatMapObservable { map ->
            viewModel.mapType.doOnNext {
                map.mapType = it
            }
        }.subscribe().disposedBy(disposeBag)
    }

    private fun updateMarkers(map: GoogleMap, places: List<Pair<Place, Float>>) {
        println("Updating markers $map")
        Log.d("refresh","refresh");
        val markers = places.map { (place, _) ->
            MarkerOptions().position(LatLng(place.latitude, place.longitude))
                    .title(place.name)
                    .snippet(place.stateString())
                    .icon(getMarkerIcon(place))
        }
        map.clear()
        markers.forEach { map.addMarker(it) }
    }

    override fun onPause() {
        super.onPause()
        disposeBag.clear()
    }
}

fun getMarkerIcon(place: Place) = BitmapDescriptorFactory.fromResource(when (place.state) {
    State.free -> R.drawable.pin_free
    State.occupied -> R.drawable.pin_occupied
    State.abuse -> R.drawable.pin_abuse
    State.maintenance -> R.drawable.pin_maintenance
})


fun Place.stateString() = when(state) {
    State.abuse -> "Occupée abusivement"
    State.occupied -> "Occupée"
    State.free -> "Libre"
    State.maintenance -> "En maintenance"
}

class PlacesMapViewModel(placeService: PlaceService, userPreferences: UserPreferences)  {
    val places = placeService.managedPlaces
    val mapType = BehaviorSubject.createDefault(userPreferences.mapType)

    init {
        mapType.subscribe { userPreferences.mapType = it }
    }
}