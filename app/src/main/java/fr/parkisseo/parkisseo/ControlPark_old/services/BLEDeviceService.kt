package fr.parkisseo.parkisseo.ControlPark_old.services

import android.bluetooth.*
import android.content.Context
import io.reactivex.Completable
import io.reactivex.CompletableEmitter
import io.reactivex.Observable
import io.reactivex.subjects.BehaviorSubject
import timber.log.Timber
import java.util.*

class BLEDeviceService(context: Context, device: BluetoothDevice) : BluetoothGattCallback() {
    private var gatt: BluetoothGatt? = null
    private var writeCompletableEmitter: CompletableEmitter? = null
    val connection: Observable<Boolean>
    init {
        connection = BehaviorSubject.createDefault(false)
        device.connectGatt(context, false, object : BluetoothGattCallback() {

            override fun onConnectionStateChange(gatt: BluetoothGatt, status: Int, newState: Int) {
                Timber.i("onConnectionStateChange $gatt $newState")
                if (this@BLEDeviceService.gatt == null) {
                    this@BLEDeviceService.gatt = gatt
                }

                when (newState) {
                    BluetoothProfile.STATE_CONNECTED -> {
                        Timber.i("Connected to $gatt")
                        gatt.discoverServices()
                        this@BLEDeviceService.gatt = gatt

                    }
                    BluetoothProfile.STATE_DISCONNECTED -> {
                        Timber.i("Disconnected to $gatt")
                        connection.onNext(false)
                        val emitter = this@BLEDeviceService.writeCompletableEmitter
                        if (emitter != null) {
                            emitter.onError(BLEException("Device disconnected"))
                            this@BLEDeviceService.writeCompletableEmitter = null
                        }
                    }
                }
            }

            override fun onServicesDiscovered(gatt: BluetoothGatt, status: Int) {
                Timber.i("onServiceDiscovered ${gatt.services}")
                connection.onNext(true)
            }

            override fun onCharacteristicWrite(gatt: BluetoothGatt?, characteristic: BluetoothGattCharacteristic?, status: Int) {
                Timber.i("onCharacteristicWrite $status")
                val emitter = writeCompletableEmitter
                if (emitter != null) {
                    if (status == BluetoothGatt.GATT_SUCCESS) {
                        emitter.onComplete()
                    } else {
                        emitter.onError(Exception("GATT error $status"))
                    }
                    this@BLEDeviceService.writeCompletableEmitter = null
                }
            }

        })
    }

    private fun getCharacteristic(): BluetoothGattCharacteristic? {
        val gatt = this.gatt ?: return null
        val service = gatt.getService(PARKISSEO_SERVICE) ?: return null
        return service.getCharacteristic(UUID.fromString("cb120002-ca32-4031-8f91-3546bd912161"))
    }



    private fun _writeCommand(command: String) = Completable.create { s ->
        val char = getCharacteristic()
        val gatt = this.gatt
        if (char != null && gatt != null) {
            this.writeCompletableEmitter = s
            char.setValue(command)
            gatt.writeCharacteristic(char)
        } else {
            s.onError(Exception("Not connected"))
        }
    }

    fun writeCommand(command: String): Completable {
        return connection.filter { it }.take(1).singleOrError().flatMapCompletable {
            println("Connected")
            _writeCommand(command)
        }
    }

    fun disconnect() {
        gatt?.disconnect()
    }
}

class BLEException(message: String): Exception(message)