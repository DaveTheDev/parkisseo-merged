package fr.parkisseo.parkisseo.ControlPark_old.ui

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.github.salomonbrys.kodein.android.KodeinSupportFragment
import com.github.salomonbrys.kodein.instance
import fr.parkisseo.parkisseo.ControlPark_old.services.SettingsService
import fr.parkisseo.parkisseo.ControlPark_old.services.disposedBy
import fr.parkisseo.parkisseo.ControlPark_old.services.startActivity
import fr.parkisseo.parkisseo.R
import io.reactivex.disposables.CompositeDisposable
import timber.log.Timber
import kotlinx.android.synthetic.main.settings.view.*

class SettingsFragment : KodeinSupportFragment() {
    val settingsService: SettingsService by instance()
    val disposeBag = CompositeDisposable()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.settings, container, false)
    }

    override fun onResume() {
        super.onResume()
        settingsService.settings.subscribe { settings ->
            view?.notificationSwitch?.isChecked = settings.notifEnabled
            view?.adminBLEButton?.visibility = if (settings.bleAdmin) View.VISIBLE else View.GONE
            Timber.i("Settings $settings")
        }.disposedBy(disposeBag)

        settingsService.loading.subscribe { loading ->
            view?.notificationSwitch?.isEnabled = !loading
            view?.progressBarSettings?.visibility = if (loading) View.VISIBLE else View.GONE
        }.disposedBy(disposeBag)

        view?.notificationSwitch?.setOnCheckedChangeListener { _, checked ->
            settingsService.updateNotification(checked)
        }

        view?.adminBLEButton?.setOnClickListener {
            activity!!.startActivity<PlaceBLEListActivity>()
        }
    }

    override fun onPause() {
        super.onPause()
        disposeBag.clear()
    }
}