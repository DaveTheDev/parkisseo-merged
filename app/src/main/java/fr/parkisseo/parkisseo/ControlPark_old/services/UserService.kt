package fr.parkisseo.parkisseo.ControlPark_old.services

import android.content.SharedPreferences
import android.util.Base64
import io.reactivex.Observable
import io.reactivex.subjects.BehaviorSubject
import timber.log.Timber


data class Credentials(val email: String, val password: String) {
    fun toBasicAuth(): String {
        val credentials = email + ":" + password
        Timber.wtf("creden: " + credentials);
        return  " Basic " + Base64.encodeToString(credentials.toByteArray(), Base64.NO_WRAP)
    }
}

class UserService(val preferences: SharedPreferences) {

    private var loggedIn = BehaviorSubject.createDefault(_isLoggedIn())

    var credentials: Credentials?
        get() {
            val email: String? = preferences.getString("email", null)
            val password: String? = preferences.getString("password", null)
            if (email != null && password != null) {
                return Credentials(email, password)
            } else return null
        }
        set(value) {
            preferences.edit()
                    .putString("email", value?.email)
                    .putString("password", value?.password)
                    .apply()

            loggedIn.onNext(value != null)
        }


    private fun _isLoggedIn(): Boolean {
        return preferences.getString("email", null) != null
    }

    val observeLogin: Observable<Boolean>
        get() = loggedIn

    val isLoggedIn: Boolean
        get() = loggedIn.value



}


