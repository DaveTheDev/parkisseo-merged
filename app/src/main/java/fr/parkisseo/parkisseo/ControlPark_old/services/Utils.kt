package fr.parkisseo.parkisseo.ControlPark_old.services

import android.R
import android.app.Activity
import android.app.ProgressDialog
import android.content.Context
import android.content.Intent
import android.support.v4.app.Fragment
import android.support.v7.app.AppCompatActivity
import android.text.format.DateUtils
import android.util.Patterns
import android.widget.Toast
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.LatLng
import com.squareup.moshi.FromJson
import com.squareup.moshi.JsonAdapter
import com.squareup.moshi.Moshi
import com.squareup.moshi.ToJson
import io.reactivex.Completable
import io.reactivex.Flowable
import io.reactivex.Observable
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable
import io.reactivex.functions.BiFunction
import timber.log.Timber
import java.text.SimpleDateFormat
import java.util.*
import java.util.concurrent.TimeUnit

inline fun <reified T> Moshi.adapter(): JsonAdapter<T> {
    return adapter(T::class.java)!!
}

fun <T> Single<T>.subscribeWithProgress(context: Context, message: String,
                                        title: String = "Chargement",
                                        successMessage: String? = null,
                                        errorMessage: (Throwable) -> String = { context.getString(fr.parkisseo.parkisseo.R.string.error_occured)},
                                        successBlock: () -> Unit = {}) {
    val dialog = ProgressDialog.show(context, title, message)
    observeOn(AndroidSchedulers.mainThread()).subscribe({
        if (successMessage != null) {
            Toast.makeText(context, successMessage, Toast.LENGTH_SHORT).show()
        }
        dialog.dismiss()
        successBlock()
    }, {
        Toast.makeText(context, errorMessage(it), Toast.LENGTH_SHORT).show()
        dialog.dismiss()
    })
}

fun Completable.subscribeWithProgress(context: Context, message: String,
                                      title: String = "Chargement",
                                      successMessage: String? = null,
                                      errorMessage: (Throwable) -> String = { context.getString(fr.parkisseo.parkisseo.R.string.error_occured) },
                                      successBlock: () -> Unit = {}) {
    val dialog = ProgressDialog.show(context, title, message)
    observeOn(AndroidSchedulers.mainThread()).subscribe({
        if (successMessage != null) {
            Toast.makeText(context, successMessage, Toast.LENGTH_SHORT).show()
        }
        dialog.dismiss()
        successBlock()
    }, {
        Toast.makeText(context, errorMessage(it), Toast.LENGTH_SHORT).show()
        dialog.dismiss()
    })
}

fun String.isEmail(): Boolean {
    return Patterns.EMAIL_ADDRESS.matcher(this).matches()
}

inline fun <reified T : Activity> Activity.startActivity(): Unit {
    startActivity(Intent(this, T::class.java))
}

inline fun <reified T : Activity> Activity.startActivity(f: Intent.() -> Unit): Unit {
    val intent = Intent(this, T::class.java)
    intent.f()
    startActivity(intent)
}

fun AppCompatActivity.pushFragment(layout: Int, fragment: Fragment, name: String? = null) {
    supportFragmentManager
            .beginTransaction()
            .setCustomAnimations(R.anim.fade_in, R.anim.fade_out, R.anim.fade_in, R.anim.fade_out)
            .replace(layout, fragment)
            .addToBackStack(name)
            .commit()
}


fun Disposable.disposedBy(bag: CompositeDisposable) {
    bag.add(this)
}

fun <T> Observable<T>.onErrorComplete(): Observable<T> =
        onErrorResumeNext { _: Throwable -> Observable.empty<T>() }


fun <T> Single<T>.logOperation(opName: String): Single<T> {
    return doOnSubscribe {
        Timber.i("$opName started")
    }.doAfterSuccess {
        Timber.i("$opName success")
    }.doOnError { e ->
        Timber.e(e, "$opName error")
    }
}


fun Completable.logOperation(opName: String): Completable {
    return doOnSubscribe {
        Timber.i("$opName started")
    }.doOnComplete {
        Timber.i("$opName success")
    }.doOnError { e ->
        Timber.e(e, "$opName error")
    }
}


fun <T> Observable<T>.logOperation(opName: String): Observable<T> {
    return doOnSubscribe {
        Timber.i("$opName started")
    }.doAfterNext {
        Timber.i("$opName success")
    }.doOnError { e ->
        Timber.e(e, "$opName error")
    }
}

fun <T> Observable<T>.retryWithExponentialBackoff(retries: Int = 3, base: Int = 1): Observable<T> {
    return retryWhen {
        it.zipWith(Observable.range(1, retries), BiFunction { _: Throwable, i: Int -> i })
                .flatMap { retryCount ->
                    val retryTime = Math.pow((base * 2).toDouble(), retryCount.toDouble()).toLong()
                    Timber.d("Retry count : $retryCount, Retry time $retryTime")
                    Observable.timer(retryTime, TimeUnit.SECONDS)
                }
    }
}

fun <T> Single<T>.retryWithExponentialBackoff(retries: Int = 3, base: Int = 1): Single<T> {
    return retryWhen {
        it.zipWith(Flowable.range(1, retries), BiFunction { _: Throwable, i: Int -> i })
                .flatMap { retryCount ->
                    val retryTime = Math.pow((base * 2).toDouble(), retryCount.toDouble()).toLong()
                    Timber.d("Retry count : $retryCount, Retry time $retryTime")
                    Flowable.timer(retryTime, TimeUnit.SECONDS)
                }
    }
}

fun Completable.retryWithExponentialBackoff(retries: Int = 3, base: Int = 1): Completable {
    return retryWhen {
        it.zipWith(Flowable.range(1, retries), BiFunction { _: Throwable, i: Int -> i })
                .flatMap { retryCount ->
                    val retryTime = Math.pow((base * 2).toDouble(), retryCount.toDouble()).toLong()
                    Timber.d("Retry count : $retryCount, Retry time $retryTime")
                    Flowable.timer(retryTime, TimeUnit.SECONDS)
                }
    }
}

fun formatDist(distance: Double): String {
    val shortUnit = "m"
    val longUnit = "km"
    val scaleUnit = 1000.0

    // distance shouldn't be negative
    val distanceAbs = Math.abs(distance)

    // conversion and formatting
    if (Math.round(distanceAbs) < scaleUnit) {
        // display as short unit, as integer
        return String.format(Locale.getDefault(), "%1\$d%2\$s",
                Math.round(distanceAbs), shortUnit)
    } else {
        val scaledDistance = distanceAbs / scaleUnit
        // round to one decimal and check if it is
        // smaller than a 1 decimal difference
        if (Math.round(scaledDistance * 10.0) / 10.0 < 10.0) {
            // display as long unit, with 1 decimal
            return String.format(
                    Locale.getDefault(), "%1$,.1f%2\$s",
                    scaledDistance, longUnit)
        } else {
            // display as long unit, as integer
            return String.format(
                    Locale.getDefault(), "%1$,d%2\$s",
                    Math.round(scaledDistance), longUnit)
        }
    }
}

fun formatTimeAgo(date: Date, now: Date = Date()): String {
    return DateUtils.getRelativeTimeSpanString(date.time, now.time, DateUtils.MINUTE_IN_MILLIS).toString()
}


class DateAdapter {
    private fun parseIso8601Date(s: String): Date {
        val df = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'")
        df.timeZone = TimeZone.getTimeZone("UTC")
        return df.parse(s)
    }

    @FromJson fun fromJson(date: String?): Date? {
        return date?.let { parseIso8601Date(it) }
    }

    @ToJson fun toJson(d: Date): String {
        return "TODO"
    }
}

fun SupportMapFragment.getMap() = Single.create<GoogleMap> { s ->
    getMapAsync {
        if (!s.isDisposed) s.onSuccess(it)
    }
}


fun GoogleMap.mapCenter() = Observable.create<LatLng> { s ->
    setOnCameraMoveListener {
        if (!s.isDisposed) s.onNext(cameraPosition.target)
    }
    s.onNext(cameraPosition.target)
    s.setCancellable {
        setOnCameraMoveListener(null)
    }
}

fun GoogleMap.userLocation() = Observable.create<LatLng> { s ->
    setOnMyLocationChangeListener { loc ->
        if (!s.isDisposed) s.onNext(LatLng(loc.latitude, loc.longitude))
    }

    s.setCancellable {
        setOnMyLocationChangeListener(null)
    }
}


fun String.trimLeadingZeros(): String {
    var result = ""
    this.forEach { c ->
        if (!result.isEmpty() || c != '0') {
            result += c
        }
    }
    return result
}