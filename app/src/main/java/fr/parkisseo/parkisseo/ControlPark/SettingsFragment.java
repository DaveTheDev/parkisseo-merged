package fr.parkisseo.parkisseo.ControlPark;


import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.SwitchCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;

import fr.parkisseo.parkisseo.ControlPark.BLE.BLEMainActivity;
import fr.parkisseo.parkisseo.ControlPark_old.services.SettingsService;
import fr.parkisseo.parkisseo.ControlPark_old.services.UserSettings;
import fr.parkisseo.parkisseo.ControlPark_old.services.UtilsKt;
import fr.parkisseo.parkisseo.ControlPark_old.ui.PlaceBLEListActivity;
import fr.parkisseo.parkisseo.R;
import io.reactivex.Observer;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Consumer;
import kotlin.jvm.internal.Intrinsics;

public class SettingsFragment extends Fragment {

    SettingsService settingsService;
    CompositeDisposable disposeBag = new CompositeDisposable();

    boolean isAdmin;

    public SettingsFragment() {
        // Required empty public constructor
    }

    @SuppressLint("ValidFragment")
    public SettingsFragment(boolean isAdmin)
    {
        this.isAdmin = isAdmin;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.settings, container, false);
        Button adminBLE = view.findViewById(R.id.adminBLEButton);

        if (isAdmin)
            adminBLE.setVisibility(View.VISIBLE);
        else
            adminBLE.setVisibility(View.GONE);

        adminBLE.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(v.getContext(),BLEMainActivity.class));
            }
        });


        return view;
    }



//    @Override
//    public void onPause() {
//        super.onPause();
//        disposeBag.clear();
//    }
//
//    @Override
//    public void onResume() {
//        super.onResume();
//
//        Disposable disposable = settingsService.getSettings().subscribe(new Consumer<UserSettings>() {
//            @Override
//            public void accept(UserSettings userSettings) throws Exception {
//                Intrinsics.checkParameterIsNotNull(userSettings, "settings");
//                View view = SettingsFragment.this.getView();
//                if (view != null) {
//                    SwitchCompat switchCompat = (SwitchCompat)view.findViewById(R.id.notificationSwitch);
//                    if (switchCompat != null) {
//                        switchCompat.setChecked(userSettings.getNotifEnabled());
//                    }
//                }
//
//                if (view != null) {
//                    Button adminBLEButton = (Button)view.findViewById(R.id.adminBLEButton);
//                    if (adminBLEButton != null) {
//                        adminBLEButton.setVisibility(userSettings.getBleAdmin() ? View.VISIBLE : View.GONE);
//                    }
//                }
//            }
//        });
//        //Intrinsics.checkExpressionValueIsNotNull(var10000, "settingsService.settings…ngs $settings\")\n        }");
//        UtilsKt.disposedBy(disposable, this.disposeBag);
//
//
//        disposable = settingsService.getLoading().subscribe(new Consumer<Boolean>() {
//            @Override
//            public void accept(Boolean loading) throws Exception {
//                View view = SettingsFragment.this.getView();
//                if (view != null) {
//                    SwitchCompat switchCompat = (SwitchCompat)view.findViewById(R.id.notificationSwitch);
//                    if (switchCompat != null) {
//                        switchCompat.setEnabled(!loading);
//                    }
//                }
//
//                if (view != null) {
//                    ProgressBar progressBar = (ProgressBar)view.findViewById(R.id.progressBarSettings);
//                    if (progressBar != null) {
//                        progressBar.setVisibility(loading ? View.VISIBLE : View.GONE);
//                    }
//                }
//            }
//        });
//
//        disposable = settingsService.getLoading().subscribe((Consumer)(new Consumer() {
//            // $FF: synthetic method
//            // $FF: bridge method
//            public void accept(Object var1) {
//                this.accept((Boolean)var1);
//            }
//
//            public final void accept(boolean loading) {
//
//
//            }
//        }));
//        Intrinsics.checkExpressionValueIsNotNull(var10000, "settingsService.loading.… else View.GONE\n        }");
//        UtilsKt.disposedBy(var10000, this.disposeBag);
//
//                settingsService.settings.subscribe { settings ->
//                view?.notificationSwitch?.isChecked = settings.notifEnabled
//            view?.adminBLEButton?.visibility = if (settings.bleAdmin) View.VISIBLE else View.GONE
//            Timber.i("Settings $settings")
//        }.disposedBy(disposeBag)
//
//        settingsService.loading.subscribe { loading ->
//                view?.notificationSwitch?.isEnabled = !loading
//            view?.progressBarSettings?.visibility = if (loading) View.VISIBLE else View.GONE
//        }.disposedBy(disposeBag)
//
//        view?.notificationSwitch?.setOnCheckedChangeListener { _, checked ->
//                settingsService.updateNotification(checked)
//        }
//
//        view?.adminBLEButton?.setOnClickListener {
//            activity!!.startActivity<PlaceBLEListActivity>()
//        }
//    }
}
