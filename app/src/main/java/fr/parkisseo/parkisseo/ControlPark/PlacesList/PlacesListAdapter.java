package fr.parkisseo.parkisseo.ControlPark.PlacesList;

import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.hereshem.lib.recycler.MyViewHolder;

import java.text.DecimalFormat;

import fr.parkisseo.parkisseo.ControlPark.ControlParkMainActivity;
import fr.parkisseo.parkisseo.R;

public class PlacesListAdapter extends MyViewHolder<Place> {

    private TextView place_name, place_info;
    private RelativeLayout relative_place;
    private ImageView place_state;



    public PlacesListAdapter(View itemView) {
        super(itemView);

        place_name = itemView.findViewById(R.id.textViewName);

        place_info = itemView.findViewById(R.id.textViewInfo);

        place_state = itemView.findViewById(R.id.place_state);

        relative_place = itemView.findViewById(R.id.relative_placeList);
    }

    @Override
    public void bindView(Place item) {
        try
        {
            place_name.setText(item.getName());

            DecimalFormat formatter = new DecimalFormat("#,###,###");
            String distance = formatter.format(item.getDistance());

            place_info.setText(item.getLastEventDate() + "\n" + distance + " Km");

            PlaceDrawable placeDrawable = new PlaceDrawable(item);
            place_state.setImageDrawable(placeDrawable);

        }catch (Exception e)
        {
            e.printStackTrace();
        }



    }
}



