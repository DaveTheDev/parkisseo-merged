package fr.parkisseo.parkisseo.ControlPark.BLE;

import android.app.Activity;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ProgressBar;

import com.hereshem.lib.recycler.MyRecyclerView;
import com.hereshem.lib.recycler.RecyclerViewAdapter;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import fr.parkisseo.parkisseo.ControlPark.TempNextBLE;
import fr.parkisseo.parkisseo.ControlPark_old.ui.BleListAdapter;
import fr.parkisseo.parkisseo.ControlPark_old.ui.PlaceBLEDetailActivity;
import fr.parkisseo.parkisseo.R;

import static fr.parkisseo.parkisseo.ControlPark.PlacesList.PlaceListFragment.places_list;

public class BLEMainActivity extends AppCompatActivity {

    public static final int REQUEST_ENABLE_BT = 1;
    List<BLE_Device> mBTDevicesArrayList;
    HashMap<String, BLE_Device> mBTDevicesHashMap;
    RecyclerViewAdapter bleListAdapter;

    private BroadcastReceiver_BTState mBTStateUpdateReceiver;
    private Scanner_BLE mBLeScanner;

    public static ProgressBar refreshProgress;
    public static ImageButton refresh_btn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_blemain);

        // Use this check to determine whether BLE is supported on the device.
        if (!getPackageManager().hasSystemFeature(PackageManager.FEATURE_BLUETOOTH_LE)) {
            Utils.toast(getApplicationContext(), "BLE not supported");
            finish();
        }



        //set the toolbar name
        try {
            getSupportActionBar().setTitle("Administration BLE");
        } catch (NullPointerException e)
        {
            e.printStackTrace();
        }


        //for the refresh button
        refreshProgress = findViewById(R.id.progressBar);
        refresh_btn = findViewById(R.id.refresh_btn);
        refresh_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showProgressCircle();
                startScan();
            }
        });


        //instantiate the list, recyclerview
        mBTDevicesArrayList = new ArrayList<>();

        MyRecyclerView recyclerView = findViewById(R.id.recycler_BLE);
        bleListAdapter = new RecyclerViewAdapter(this, mBTDevicesArrayList,
                BLEListAdapter.class, R.layout.ble_item);
        recyclerView.setAdapter(bleListAdapter);

        mBTDevicesHashMap = new HashMap<>();

        //initiate the receiver and the scanner class
        mBTStateUpdateReceiver = new BroadcastReceiver_BTState(this);
        mBLeScanner = new Scanner_BLE(this, 5000, -75);

        //start scanning
        if (!mBLeScanner.isScanning())
        {
            showProgressCircle();
            startScan();

        }

        //on item click
        recyclerView.setOnItemClickListener(new MyRecyclerView.OnItemClickListener() {
            @Override
            public void onItemClick(int position) {
                Intent intent = new Intent (BLEMainActivity.this, PlaceBLEDetailActivity.class);
                intent.putExtra("device", mBTDevicesArrayList.get(position).getBluetoothDevice())
                        .putExtra("title", mBTDevicesArrayList.get(position).getName());
//                        .putExtra("place", result.getResultPlace());
                startActivity(intent);
            }
        });
    }

    //show and hide the progress circle
    public static void showProgressCircle()
    {
        refreshProgress.setVisibility(View.VISIBLE);
        refresh_btn.postDelayed(new Runnable() {
            public void run()
            {
                refresh_btn.setVisibility(View.INVISIBLE);
            }
        },0);
    }

    public static void stopProgressCircle()
    {
        refreshProgress.setVisibility(View.INVISIBLE);
        refresh_btn.postDelayed(new Runnable() {
            public void run()
            {
                refresh_btn.setVisibility(View.VISIBLE);
            }
        },0);
    }

    @Override
    protected void onStart() {
        super.onStart();
        registerReceiver(mBTStateUpdateReceiver, new IntentFilter(BluetoothAdapter.ACTION_STATE_CHANGED));
    }

    @Override
    protected void onResume() {
        super.onResume();
        registerReceiver(mBTStateUpdateReceiver, new IntentFilter(BluetoothAdapter.ACTION_STATE_CHANGED));
    }

    @Override
    protected void onPause() {
        super.onPause();
        stopScan();
    }

    @Override
    protected void onStop() {
        super.onStop();
        unregisterReceiver(mBTStateUpdateReceiver);
        stopScan();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        //check the bluetooth request
        if (requestCode == REQUEST_ENABLE_BT) {
            // Make sure the request was successful
            if (resultCode == RESULT_OK) {
                showProgressCircle();
                startScan();
            }
            //unsuccessful
            else if (resultCode == RESULT_CANCELED)
            {
                //bluetooth request was denied
                Utils.toast(getApplicationContext(), "Please turn on Bluetooth");
                finish();
            }
        }

    }

    //add device to the list from the scanner
    public void addDevice(BluetoothDevice device, int rssi)
    {
        String address = device.getAddress();
        if (!mBTDevicesHashMap.containsKey(address)) {
            BLE_Device btleDevice = new BLE_Device(device);
            btleDevice.setRSSI(rssi);
            btleDevice.setName(device.getName());

            mBTDevicesHashMap.put(address, btleDevice);
            mBTDevicesArrayList.add(btleDevice);
        }
        else {
            mBTDevicesHashMap.get(address).setRSSI(rssi);
        }

        //update the list
        bleListAdapter.notifyDataSetChanged();
    }

    //start the scan, clear the list and make room for new data
    public void startScan()
    {
        mBTDevicesArrayList.clear();
        mBTDevicesHashMap.clear();
        mBLeScanner.start();
    }

    //stop the scan
    public void stopScan() {
        mBLeScanner.stop();
    }
}
