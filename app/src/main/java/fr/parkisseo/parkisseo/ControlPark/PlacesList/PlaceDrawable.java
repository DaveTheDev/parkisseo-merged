package fr.parkisseo.parkisseo.ControlPark.PlacesList;

import android.annotation.SuppressLint;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.ColorFilter;
import android.graphics.ImageFormat;
import android.graphics.Paint;
import android.graphics.PixelFormat;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.drawable.Drawable;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

public class PlaceDrawable extends Drawable {

    Paint placePaint;

    Place place;
    public PlaceDrawable(Place place)
    {
        placePaint = new Paint();
        placePaint.setStyle(Paint.Style.FILL);
        placePaint.setColor(Color.argb(255,108,114,122));
        placePaint.setAntiAlias(true);

        this.place = place;
    }

    @Override
    public void draw(@NonNull Canvas canvas) {

        float inset = getBounds().height() * 0.1f;
        RectF rect = new RectF(getBounds());

        rect.inset(inset, inset);


        placePaint.setColor(place.toColor());
        canvas.drawCircle(rect.centerX(), rect.centerY(), rect.width() / 2, placePaint);
    }

    @Override
    public void setAlpha(int alpha) {

    }

    @Override
    public void setColorFilter(@Nullable ColorFilter colorFilter) {

    }

    @SuppressLint("WrongConstant")
    @Override
    public int getOpacity() {
        return 255;
    }
}




