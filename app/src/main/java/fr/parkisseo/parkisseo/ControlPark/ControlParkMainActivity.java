package fr.parkisseo.parkisseo.ControlPark;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.provider.Settings;
import android.support.design.widget.TabLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AlertDialog;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.util.TypedValue;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.maps.GoogleMap;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

import fr.parkisseo.parkisseo.ControlPark.PlacesList.Place;
import fr.parkisseo.parkisseo.ControlPark.PlacesList.PlaceListFragment;
import fr.parkisseo.parkisseo.MainActivity;
import fr.parkisseo.parkisseo.R;

public class ControlParkMainActivity extends AppCompatActivity {

    public static boolean gotData = false;
    public static Context context;
    private SharedPreferences sharedPreferences;
    private LocationManager locationManager;
    private ViewPager viewPager;
    private TabFragmentAdapter tabFragmentAdapter;
    private TabLayout tabLayout;
    static ProgressBar refreshProgress;
    static ImageButton refresh_btn;
    public static Location userLocation;
    private LocationListener locationListener;
    public static boolean sortByDistance = true;


    final Handler handler = new Handler();
    final int delay = 20000; //milliseconds

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_controlpark);

        context = this;
        //set the toolbar so that it shows the menu
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getUserLocation();

        sharedPreferences = getSharedPreferences("term_shared",Context.MODE_PRIVATE);
        boolean term_state = sharedPreferences.getBoolean("isFirstTime",true);
        boolean isAdmin = sharedPreferences.getBoolean("isAdmin", false);
        Log.d("isAdmin", isAdmin+"");

        if(term_state){
            privacyAccept();
        }

        //for the refresh button
        refreshProgress = findViewById(R.id.progressBar);

        // Get Location Manager and check for GPS & Network location services
        locationManager  = (LocationManager) getSystemService(LOCATION_SERVICE);
        if (!locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER) ||
                !locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER))
        {

            //show a dialog if the location services aren't active
            AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(this);
            dialogBuilder.setTitle(getString(R.string.location_services_notActive));
            dialogBuilder.setCancelable(false);
            dialogBuilder.setMessage(getString(R.string.pleaseEnable_location_GPS));
            dialogBuilder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    Intent intent =  new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                    startActivity(intent);
                }
            });
            dialogBuilder.create();
            dialogBuilder.show();
        }




        //instantiate the data
        viewPager = findViewById(R.id.viewPager);
        tabLayout = findViewById(R.id.tabLayout);


        //add the fragments to the tabFragmentAdapter
        tabFragmentAdapter = new TabFragmentAdapter(getSupportFragmentManager());
        tabFragmentAdapter.addFragment(new PlaceListFragment(this));
        tabFragmentAdapter.addFragment(new MapFragment());
        tabFragmentAdapter.addFragment(new SettingsFragment(isAdmin));


        //set the adapter and setup the viewpager for swiping
        viewPager.setAdapter(tabFragmentAdapter);
        tabLayout.setupWithViewPager(viewPager);

        //put the icons
        tabLayout.getTabAt(0).setIcon(R.drawable.ic_action_format_list_bulleted);
        tabLayout.getTabAt(1).setIcon(R.drawable.ic_action_location);
        tabLayout.getTabAt(2).setIcon(R.drawable.ic_action_settings);



        //check for location permissions
        if (!(ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION)
                == PackageManager.PERMISSION_GRANTED))
        {
            ActivityCompat.requestPermissions(this,
                    new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                    1);
        }
        else
        {
            //get location updates since we got permission acceptance
            locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 0, 0, locationListener);
        }




        //refresh button and what should happen when clicked
        refresh_btn = findViewById(R.id.refresh_btn);
        refresh_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showProgressCircle();
                handler.removeCallbacksAndMessages(null);
                handler.postDelayed(runnable,delay);
                if (MainActivity.userLatLng!=null || MapFragment.userLatLng!=null)
                {
                    PlaceListFragment.getPlaces(ControlParkMainActivity.this);
                }
            }
        });



    }

    //add location listener for user location
    void getUserLocation()
    {
        locationListener = new LocationListener() {
            public void onLocationChanged(Location location) {
                userLocation = location;
            }

            @Override
            public void onProviderDisabled(String provider) {
                // TODO Auto-generated method stub

            }
            @Override
            public void onProviderEnabled(String provider) {
                // TODO Auto-generated method stub

            }
            @Override
            public void onStatusChanged(String provider, int status, Bundle extras) {
                // TODO Auto-generated method stub

            }
        };

    }



    //show and hide the progress circle
    public static void showProgressCircle()
    {
        refreshProgress.setVisibility(View.VISIBLE);
        refresh_btn.postDelayed(new Runnable() {
            public void run()
            {
                refresh_btn.setVisibility(View.INVISIBLE);
            }
        },0);
    }

    public static void stopProgressCircle()
    {
        refreshProgress.setVisibility(View.INVISIBLE);
        refresh_btn.postDelayed(new Runnable() {
            public void run()
            {
                refresh_btn.setVisibility(View.VISIBLE);
            }
        },0);
    }


    Runnable runnable = new Runnable() {
        @Override
        public void run() {
            //if the location is not null, get the places
            if (MainActivity.userLatLng!=null || MapFragment.userLatLng!=null)
            {
                PlaceListFragment.getPlaces(ControlParkMainActivity.this);
            }
            Log.d("HANDLER","Called from handler");
            handler.postDelayed(this, delay);
        }
    };

    @Override
    protected void onResume() {
        super.onResume();

        handler.postDelayed(runnable, delay);
    }

    @Override
    protected void onStop() {
        super.onStop();
        ControlParkMainActivity.gotData = false;
        handler.removeCallbacksAndMessages(null);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu)
    {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.main_menu_controlpark, menu);
        return true;
    }





    //when map options is clicked
    public void onMapOptionsSelected(MenuItem item){

        switch (item.getItemId())
        {
            case R.id.itemMapTypeNormal:
                if (MapFragment.mMap!=null)
                {
                    item.setChecked(true);
                    MapFragment.mMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
                }
                break;

            case R.id.itemMapTypeHybrid:
                if (MapFragment.mMap!=null)
                {
                    item.setChecked(true);
                    MapFragment.mMap.setMapType(GoogleMap.MAP_TYPE_SATELLITE);
                }
                break;
        }
    }

    //when sorting options are clicked
    public void onSortOptionsClicked(MenuItem item)
    {
        switch (item.getItemId())
        {
            case R.id.itemSortNearest:
                item.setChecked(true);
                sortByDistanceOrState(true);
                break;

            case R.id.itemSortAbusive:
                item.setChecked(true);
                sortByDistanceOrState(false);
                break;
        }
    }

    //sort the list by distance or state.
    //if sortByDistance is true, it will sort by distance, else sort by state
    public static void sortByDistanceOrState(boolean sortByDistance)
    {
        if (sortByDistance)
        {
            if (!PlaceListFragment.places_list.isEmpty())
            {
                PlaceListFragment.places_list.sort(new Comparator<Place>() {
                    @Override
                    public int compare(Place o1, Place o2) {
                        if (o1.getDistance() > o2.getDistance()) {
                            return 1;
                        }
                        else if (o1.getDistance() < o2.getDistance()) {
                            return -1;
                        }
                        else {
                            return 0;
                        }
                    }
                });
                ControlParkMainActivity.sortByDistance = true;
            }
        }
        else
        {
            //sort by abuse
            if (!PlaceListFragment.places_list.isEmpty())
            {
                PlaceListFragment.places_list.sort(new Comparator<Place>() {
                    @Override
                    public int compare(Place o1, Place o2) {
                        if (o1.getTimeStamp().compareTo(o2.getTimeStamp())>0) {
                            return 1;
                        }
                        else if (o1.getTimeStamp().compareTo(o2.getTimeStamp())<0) {
                            return -1;
                        }
                        else {
                            return 0;
                        }
                    }
                });
                ControlParkMainActivity.sortByDistance = false;

            }
        }

        //update the list
        ((Activity)context).runOnUiThread(new Runnable() {
            @Override
            public void run() {
                PlaceListFragment.adapter.notifyDataSetChanged();
            }
        });

    }


    //on menu option selected
    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        // Handle item selection
        switch (item.getItemId())
        {
            case R.id.item_disconnect:
            {
                logoutDialog();
                return true;
            }

            case R.id.privacy:
            {
                privacyPolicy();
                return true;
            }

            case R.id.action_switchToParkiCity:
            {
                finish();
                return true;
            }

            default:
                return super.onOptionsItemSelected(item);
        }
    }

    //popup with privacy policy
    private void privacyPolicy() {
        AlertDialog.Builder alert = new AlertDialog.Builder(this);
        WebView wv = new WebView(this);

        alert.setTitle(getString(R.string.privacy_policy));
        alert.setCancelable(false);
        alert.setNegativeButton(getResources().getString(R.string.close_key),null);
        alert.setView(wv);


        wv.getSettings().setJavaScriptEnabled(true);
        wv.setVerticalScrollBarEnabled(true);

//        wv.loadUrl("https://sites.google.com/view/abedtest/home?authuser=1");
        wv.loadUrl("https://parkisseo.launchaco.com/");
        wv.setWebViewClient(new WebViewClient() {
            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                view.loadUrl(url);

                return true;
            }
        });

        alert.show();
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();

        SharedPreferences app_preferences = PreferenceManager.getDefaultSharedPreferences(this);
        //make the privacy policy popup show up if the user didn't click on remember me
        boolean rememberMe = app_preferences.getBoolean("rememberMe",false);
        if (!rememberMe)
            setFirstTime(true);
    }

    //logout dialog
    private void logoutDialog() {
        (new AlertDialog.Builder(this)
                .setTitle(getResources().getString(R.string.ask_disconnect))
                .setNegativeButton(getResources().getString(R.string.no_key), null)
                .setPositiveButton(getResources().getString(R.string.yes_key), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        setFirstTime(true);
                        logout();
                    }
                })).show();
    }

    //log out
    private void logout()
    {
        SharedPreferences app_preferences = PreferenceManager.getDefaultSharedPreferences(this);
        SharedPreferences.Editor editor = app_preferences.edit();
        editor.putBoolean("rememberMe", false);
        editor.apply();
        finish();
        startActivity(new Intent(this, MainActivity.class));
    }

    //set first time for the privacy policy
    private void setFirstTime(boolean state)
    {
        SharedPreferences.Editor editor=sharedPreferences.edit();
        editor.putBoolean("isFirstTime",state);
        editor.apply();
    }


    //exit the app and go back to parkicity
    private void exitApp()
    {
        moveTaskToBack(true);
        finish();
        startActivity(new Intent(this, MainActivity.class));
    }

    //show the privacy policy dialog. The layout is being built programmatically
    void privacyAccept()
    {
        //create the alert dialog
        AlertDialog.Builder alert = new AlertDialog.Builder(this);


        TextView terms= new TextView(this);
        terms.setText("\t\t\t" + getString(R.string.terms_conditions));
        terms.setTextSize(TypedValue.COMPLEX_UNIT_SP,10f);
        alert.setTitle(getString(R.string.privacy_policy));
        alert.setCancelable(false);

        alert.setPositiveButton(getResources().getString(R.string.accept_key), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                setFirstTime(false);
            }
        });

        alert.setNegativeButton(getResources().getString(R.string.decline_key), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                exitApp();
            }
        });


        //webview to view the terms and conditions
        final WebView wv = new WebView(this);
        //wv.loadUrl("https://sites.google.com/view/abedtest/home?authuser=1");
        wv.loadUrl("https://parkisseo.launchaco.com/");
        wv.setWebViewClient(new WebViewClient() {
            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                view.loadUrl(url);

                return true;
            }
        });


        wv.setVisibility(View.GONE);

        //accept button
        final Button positive = new Button(this);

        terms.setTextColor(Color.BLUE);

        terms.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (wv.getVisibility() == View.VISIBLE)
                    wv.setVisibility(View.GONE);
                else
                    wv.setVisibility(View.VISIBLE);
            }
        });

        //check box
        CheckBox cbx = new CheckBox(this);
        cbx.setText(getString(R.string.accepted_terms));




        wv.getSettings().setJavaScriptEnabled(true);
        wv.setVerticalScrollBarEnabled(true);


        LinearLayout layout = new LinearLayout(this);
        layout.setOrientation(LinearLayout.VERTICAL);
        layout.setVerticalScrollBarEnabled(true);
        layout.addView(cbx);
        layout.addView(terms);
        layout.addView(wv);


        final AlertDialog dialog = alert.create();
        dialog.setView(layout);
        dialog.show();
        dialog.getButton(AlertDialog.BUTTON_POSITIVE)
                .setEnabled(false);

        cbx.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                positive.setEnabled(isChecked);
                dialog.getButton(AlertDialog.BUTTON_POSITIVE)
                        .setEnabled(isChecked);

            }
        });
    }

    //When requesting permissions
    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        switch (requestCode)
        {
            case 1: //Location Permissions
                if (
                        ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION)
                                == PackageManager.PERMISSION_GRANTED )
                {
                    //location permission accepted
                    locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 0, 0, locationListener);
                } else {
                    //location permission rejected
                    Toast.makeText(this, getResources().getString(R.string.parkicity_location_permission), Toast.LENGTH_SHORT).show();
                }
                break;
            default:
                super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }
}




