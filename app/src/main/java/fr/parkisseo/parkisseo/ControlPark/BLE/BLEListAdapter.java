package fr.parkisseo.parkisseo.ControlPark.BLE;

import android.view.View;
import android.widget.TextView;

import com.hereshem.lib.recycler.MyViewHolder;

import fr.parkisseo.parkisseo.ControlPark.PlacesList.Place;
import fr.parkisseo.parkisseo.R;

public class BLEListAdapter extends MyViewHolder<BLE_Device> {

    private TextView device_name;

    public BLEListAdapter(View itemView) {
        super(itemView);

        device_name = itemView.findViewById(R.id.textViewName);
    }

    @Override
    public void bindView(BLE_Device item) {

        if (item.getName()!=null && item.getName().length() > 0)
            device_name.setText(item.getName());
        else
            device_name.setText(item.getAddress());

    }
}
