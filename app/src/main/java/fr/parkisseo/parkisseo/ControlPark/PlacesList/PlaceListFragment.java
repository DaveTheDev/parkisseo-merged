package fr.parkisseo.parkisseo.ControlPark.PlacesList;


import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.location.Location;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.format.DateUtils;
import android.util.AttributeSet;
import android.util.Log;
import android.util.SparseArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.google.android.gms.maps.model.LatLng;
import com.hereshem.lib.recycler.MyRecyclerView;
import com.hereshem.lib.recycler.RecyclerViewAdapter;
import com.squareup.okhttp.Callback;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.Response;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.IOException;
import java.security.acl.Group;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import fr.parkisseo.parkisseo.Application;
import fr.parkisseo.parkisseo.ControlPark.ControlParkMainActivity;
import fr.parkisseo.parkisseo.ControlPark.MapFragment;
import fr.parkisseo.parkisseo.MainActivity;
import fr.parkisseo.parkisseo.R;

/**
 * A simple {@link Fragment} subclass.
 */
@SuppressLint("ValidFragment")
public class PlaceListFragment extends Fragment implements MyRecyclerView.OnItemClickListener {


    private static MyRecyclerView recycler;
    public static RecyclerViewAdapter adapter;

    public PlaceListFragment(Context context) {
   }

   private static View view;
   public  static final List<Place> places_list = new ArrayList<>();



    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.places_list, container, false);
        getPlaces(view.getContext());

        //instantiate the adapter and recycler view
        recycler = view.findViewById(R.id.recyclerView);
        adapter = new RecyclerViewAdapter((Activity)view.getContext(),
                places_list,
                PlacesListAdapter.class, R.layout.place_item);
        adapter.setOnItemClickListener(this);
        setRetainInstance(true);
        //for using this in other classes
        this.view = view;
        return view;
    }


    //send request to get the places
    public static void getPlaces(final Context context)
    {

        try
        {
            ControlParkMainActivity.showProgressCircle();
            showDialogProgress();


            OkHttpClient client = new OkHttpClient();
            SharedPreferences app_preferences = PreferenceManager.getDefaultSharedPreferences(context) ;

            //get the login credentials
            String credentials = app_preferences.getString("Authorization", "");

            if (!credentials.isEmpty())
            {
                //build and send the request
                Request request = new Request.Builder()
                        .header("accept", "application/json")
                        .addHeader("Authorization", " Basic " + credentials)
                        .url("https://dev-api.parkisseo.com/places")
                        .build();

                client.newCall(request).enqueue(new Callback() {
                    @Override
                    public void onFailure(Request request, IOException e) {
                        e.printStackTrace();
                        ((Activity)context).runOnUiThread(new Runnable() {

                            @Override
                            public void run() {
                                //showProgress(false);
                                ControlParkMainActivity.stopProgressCircle();
                                Toast.makeText(context,
                                        context.getResources().getString(R.string.error_occured), Toast.LENGTH_LONG).show();
                            }
                        });

                    }

                    @Override
                    public void onResponse(final Response response) throws IOException {

                        if (response.code()==200)
                        {
                            //getting the response and parse it
                            String responseBody = response.body().string();
                            Log.d("***GOT RESPONSE***", responseBody);
                            parseJSONPlaces(responseBody);
                        }
                        else
                        {
                            //something went wrong
                            ((Activity)context).runOnUiThread(new Runnable() {

                                @Override
                                public void run() {
                                    //showProgress(false);
                                    ControlParkMainActivity.stopProgressCircle();
                                    Toast.makeText(context,
                                            context.getResources().getString(R.string.error_occured), Toast.LENGTH_LONG).show();
                                    progressDialog.dismiss();
                                }
                            });
                        }

                    }
                });
            }
        } catch (Exception e)
        {
            e.printStackTrace();
        }

    }




    //parsing the json data here
    private static void parseJSONPlaces(String data)
    {



        try
        {

            recycler.stopScroll();
            Place tempPlace;
            places_list.clear();
            JSONArray placesArray = new JSONArray(data);

            for (int i = 0; i < placesArray.length(); i++)
            {
                JSONObject jsonObject = (JSONObject) placesArray.get(i);

                tempPlace = new Place();

                //id
                tempPlace.setId(jsonObject.getInt("id"));

                //name
                tempPlace.setName(jsonObject.getString("name"));


                //state
                switch (jsonObject.getString("state"))
                {
                    case "free":
                        tempPlace.setState(State.free);
                        break;

                    case "abuse":
                        tempPlace.setState(State.abuse);
                        break;

                    case "maintenance":
                        tempPlace.setState(State.maintenance);
                        break;

                    case "occupied":
                        tempPlace.setState(State.occupied);
                        break;
                }



                //Latitude and longitutde
                if (jsonObject.getString("latitude").equals("null") ||
                        jsonObject.getString("longitude").equals("null"))
                {
                    //they're null, skip this place
                    continue;
                }
                else
                {
                    tempPlace.setLatitude(Double.parseDouble(jsonObject.getString("latitude")));
                    tempPlace.setLongitude(Double.parseDouble(jsonObject.getString("longitude")));
                }


                //set timestamp
                long dateToFormat = jsonObject.getLong("updated");
                tempPlace.setTimeStamp(String.valueOf(dateToFormat));

                CharSequence timeAgo = DateUtils.getRelativeTimeSpanString(dateToFormat*1000,
                        System.currentTimeMillis(), DateUtils.SECOND_IN_MILLIS);

                tempPlace.setLastEventDate(timeAgo.toString());

                LatLng userLatLng = null;
                //set distance
                if (MainActivity.userLatLng != null)
                    userLatLng = MainActivity.userLatLng;
                else if (MapFragment.userLatLng!=null)
                    userLatLng = MapFragment.userLatLng;


                tempPlace.setDistance(getDistanceBetween(userLatLng.latitude,
                        userLatLng.longitude,
                        tempPlace.getLatitude(),
                        tempPlace.getLongitude()));




                //add it to the list
                    places_list.add(tempPlace);


            }

            //the data have been appended to the list, we remove the old data
//            if (ControlParkMainActivity.gotData)
//                places_list.subList(0,placesArray.length()-1).clear();


            //if sort by distance is checked, sort by distance, else sort by abuse
            if (ControlParkMainActivity.sortByDistance)
                ControlParkMainActivity.sortByDistanceOrState(true);
            else
                ControlParkMainActivity.sortByDistanceOrState(false);


            //stop the circle
            ControlParkMainActivity.stopProgressCircle();

            //group the data by state
            groupByState();

            ((Activity)view.getContext()).runOnUiThread(new Runnable() {
                @Override
                public void run() {


                    //if the list is not empty
                    if (places_list.size() != 0)
                    {
                        if (!ControlParkMainActivity.gotData )
                        {
                            //if we didnt get the data first, set the adapter
                            ControlParkMainActivity.gotData = true;
                            recycler.setAdapter(adapter);
                        }
                        else
                        {
                            //update the list
                            recycler.setAdapter(adapter);
                            adapter.notifyDataSetChanged();
                        }
                        //update the map in the second tab
                        MapFragment.placeMarkers(places_list);
                    }

                }
            });

            progressDialog.dismiss();


        } catch (Exception e)
        {
            e.printStackTrace();
        }




    }


    @Override
    public void onResume() {
        super.onResume();
        recycler.setAdapter(adapter);
    }

    static ProgressDialog progressDialog;

    public static void showDialogProgress()
    {
        progressDialog = ProgressDialog.show(ControlParkMainActivity.context, "",
                ControlParkMainActivity.context.getResources().getString(R.string.updatingData));
        progressDialog.setCancelable(false);

    }


    //put each state in a list and then update the general list.
    //Add the data to the list and delete the old data
    private static void groupByState() {

        try {
            List<Place> free_list = new ArrayList<>();
            List<Place> occupied_list = new ArrayList<>();
            List<Place> abuse_list = new ArrayList<>();
            List<Place> maintenance_list = new ArrayList<>();

            for (int i = 0; i < places_list.size(); i++)
            {
                if (places_list.get(i).getState() == State.free)
                    free_list.add(places_list.get(i));
                else if (places_list.get(i).getState() == State.occupied)
                    occupied_list.add(places_list.get(i));
                else if (places_list.get(i).getState() == State.abuse)
                    abuse_list.add(places_list.get(i));
                else
                    maintenance_list.add(places_list.get(i));
            }

            int originalSize = places_list.size();

            places_list.clear();

            places_list.addAll(abuse_list);
            places_list.addAll(occupied_list);
            places_list.addAll(maintenance_list);
            places_list.addAll(free_list);
        } catch (Exception e)
        {
            e.printStackTrace();
        }



        //places_list.subList(0,originalSize).clear();

    }


    //This function will calculate the distance between 2 points and will return the distance in kilometers
    static int getDistanceBetween(double fromLat, double fromLong, double toLat, double toLong) {
        Location from = new Location("locationA");
        from.setLatitude(fromLat);
        from.setLongitude(fromLong);
        Location to = new Location("locationB");
        to.setLatitude(toLat);
        to.setLongitude(toLong);
        return (int)(from.distanceTo(to)*0.001);
    }


    //when an item is clicked, it should launch the map activity with the place data to put the marker
    @Override
    public void onItemClick(int position) {
        Intent intent = new Intent (getContext(), PlaceDetailActivity.class);
        intent.putExtra("place", places_list.get(position));
        startActivity(intent);
    }
}
