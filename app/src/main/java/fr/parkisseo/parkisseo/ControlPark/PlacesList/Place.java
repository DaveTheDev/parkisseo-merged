package fr.parkisseo.parkisseo.ControlPark.PlacesList;

import android.graphics.Color;
import android.support.annotation.NonNull;

import java.io.Serializable;

import fr.parkisseo.parkisseo.R;

public class Place implements Serializable
{
    private int id;
    private String name;
    private State state;
    private Double latitude, longitude;
    private String lastEventDate;
    private int distance;
    private String timeStamp;

    public String getTimeStamp() {
        return timeStamp;
    }

    public void setTimeStamp(String timeStamp) {
        this.timeStamp = timeStamp;
    }



    public int getDistance() {
        return distance;
    }

    public void setDistance(int distance) {
        this.distance = distance;
    }



    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public State getState() {
        return state;
    }

    public void setState(State state) {
        this.state = state;
    }

    public Double getLatitude() {
        return latitude;
    }

    public void setLatitude(Double latitude) {
        this.latitude = latitude;
    }

    public Double getLongitude() {
        return longitude;
    }

    public void setLongitude(Double longitude) {
        this.longitude = longitude;
    }

    public String getLastEventDate() {
        return lastEventDate;
    }

    public void setLastEventDate(String lastEventDate) {
        this.lastEventDate = lastEventDate;
    }


    //for the color next to each list item
    int toColor()
    {
        switch (getState())
        {
            case free:
                return Color.GREEN;
            case abuse:
                return Color.RED;
            case occupied:
                return Color.YELLOW;
            case maintenance:
                return Color.argb(255, 86, 161, 239);
        }
        return Color.BLACK;
    }


    //get the drawable resounce for the icon
    public int toDrawableResouce()
    {
        switch (getState())
        {
            case free:
                return R.drawable.pin_free;
            case abuse:
                return R.drawable.pin_abuse;
            case occupied:
                return R.drawable.pin_occupied;
            case maintenance:
                return R.drawable.pin_maintenance;
        }

        return 0;
    }


    //get the id of the string resource
    public int toStateStringResource()
    {
        switch (getState())
        {
            case free:
                return R.string.marker_free;
            case abuse:
                return R.string.marker_abuse;
            case occupied:
                return R.string.marker_abuse;
            case maintenance:
                return R.string.marker_maintenance;
        }
        return 0;
    }
}

//states
enum State {
    free, occupied, abuse, maintenance
}
