package fr.parkisseo.parkisseo.ControlPark.BLE;

import android.app.Activity;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothGattCharacteristic;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.view.Gravity;
import android.widget.Toast;


public class Utils {

    public static boolean checkBluetooth(BluetoothAdapter bluetoothAdapter) {

        // Ensures Bluetooth is available on the device and it is enabled. If not,
        // displays a dialog requesting user permission to enable Bluetooth.
        if (bluetoothAdapter == null || !bluetoothAdapter.isEnabled()) {
            return false;
        }
        else {
            return true;
        }
    }

    //request Bluetooth from the user
    public static void requestUserBluetooth(Activity activity) {
        Intent enableBtIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
        activity.startActivityForResult(enableBtIntent, BLEMainActivity.REQUEST_ENABLE_BT);
    }

//    public static IntentFilter makeGattUpdateIntentFilter() {
//
//        final IntentFilter intentFilter = new IntentFilter();
//
//        intentFilter.addAction(Service_BTLE_GATT.ACTION_GATT_CONNECTED);
//        intentFilter.addAction(Service_BTLE_GATT.ACTION_GATT_DISCONNECTED);
//        intentFilter.addAction(Service_BTLE_GATT.ACTION_GATT_SERVICES_DISCOVERED);
//        intentFilter.addAction(Service_BTLE_GATT.ACTION_DATA_AVAILABLE);
//
//        return intentFilter;
//    }


    public static void toast(Context context, String string) {

        Toast toast = Toast.makeText(context, string, Toast.LENGTH_SHORT);
        toast.show();
    }
}
