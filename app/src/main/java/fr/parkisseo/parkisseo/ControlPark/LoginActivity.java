package fr.parkisseo.parkisseo.ControlPark;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.BoringLayout;
import android.text.TextUtils;
import android.util.Base64;
import android.util.Log;
import android.util.Patterns;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.ScrollView;

import com.squareup.okhttp.Call;
import com.squareup.okhttp.Callback;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.Response;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Map;
import java.util.regex.Pattern;

import fr.parkisseo.parkisseo.R;

public class LoginActivity extends AppCompatActivity {

    EditText mEmailView, mPasswordView;
    CheckBox checkbox_rememberMe;
    Button btn_login;
    boolean rememberMe = false;
    ScrollView mLoginFormView;
    ProgressBar mProgressView;
    Boolean isAdmin;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        //Email
        mEmailView = findViewById(R.id.email);

        //Password
        mPasswordView = findViewById(R.id.password);


        //Remember me checkbox
        checkbox_rememberMe = findViewById(R.id.checkbox_rememberMe);
        checkbox_rememberMe.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                rememberMe = isChecked;
            }
        });

        //Login button
        btn_login = findViewById(R.id.email_sign_in_button);
        btn_login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                attemptLogin();
            }
        });


        mLoginFormView = findViewById(R.id.login_form);
        mProgressView = findViewById(R.id.login_progress);


        //see if user logged in
        SharedPreferences app_preferences = PreferenceManager.getDefaultSharedPreferences(this);
        boolean rememberMePrefs = app_preferences.getBoolean("rememberMe", false);

        //if logged in, start the main activity
        if (rememberMePrefs) {
            startMainActivity();
            overridePendingTransition(0, 0);
        }

    }

    void attemptLogin()
    {

        //Reset errors
        mEmailView.setError(null);
        mPasswordView.setError(null);

        // Store values at the time of the login attempt.
        final String email = mEmailView.getText().toString();
        final String password = mPasswordView.getText().toString();

        boolean cancel = false;
        View focusView = null;

        // Check for a valid password, if the user entered one.
        if (TextUtils.isEmpty(password)) {
            mPasswordView.setError(getString(R.string.error_invalid_password));
            focusView = mPasswordView;
            cancel = true;
        }

        // Check for a valid email address.
        if (TextUtils.isEmpty(email))
        {
            mEmailView.setError(getString(R.string.error_field_required));
            focusView = mEmailView;
            cancel = true;
        }
        else if (!isEmailValid(email))
        {
            mEmailView.setError(getString(R.string.error_invalid_email));
            focusView = mEmailView;
            cancel = true;
        }

        if (cancel) {
            // There was an error; don't attempt login and focus the first
            // form field with an error.
            focusView.requestFocus();
        }
        else
        {
            showProgress(true);
            final String credentials = email + ":" + password;

            OkHttpClient client = new OkHttpClient();

            final Request request = new Request.Builder()
                    .header("accept", "application/json")
                    .addHeader("Authorization", " Basic " + Base64.encodeToString(credentials.getBytes(), Base64.NO_WRAP))
                    .url("https://dev-api.parkisseo.com/users/self")
                    .build();

            client.newCall(request).enqueue(new Callback() {
                @Override
                public void onFailure(Request request, IOException e) {
                    e.printStackTrace();
                    runOnUiThread(new Runnable() {

                        @Override
                        public void run() {
                            showProgress(false);
                            Snackbar.make(mLoginFormView, getString(R.string.error_occured), Snackbar.LENGTH_SHORT).show();
                        }
                    });

                }

                @Override
                public void onResponse(final Response response) throws IOException {



                    String responseBody = response.body().string();
                    Log.d("***GOT RESPONSE***", responseBody);

                    try {
                        JSONObject mainObject = new JSONObject(responseBody);
                        isAdmin = mainObject.getBoolean("isAdmin");

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }




                    runOnUiThread(new Runnable() {

                        @Override
                        public void run()
                        {
                            if (response.code() == 200)
                            {

                                //LoginService loginService = new LoginService();
                                SharedPreferences app_preferences = PreferenceManager.getDefaultSharedPreferences(LoginActivity.this) ;
                                SharedPreferences.Editor editor = app_preferences.edit() ;
                                editor.putBoolean("rememberMe", rememberMe) ;
                                editor.putString("Authorization", Base64.encodeToString(credentials.getBytes(), Base64.NO_WRAP));
                                Log.d("rememberMe onBack", rememberMe+"");

                                editor.apply() ;

                                app_preferences = getSharedPreferences("term_shared",Context.MODE_PRIVATE) ;
                                editor = app_preferences.edit() ;
                                editor.putBoolean("isAdmin", isAdmin);
                                editor.apply();

                                startMainActivity();
                                //Snackbar.make(mLoginFormView, "User already logged in!", Snackbar.LENGTH_SHORT).show();
                            }
                            else if (response.code() == 500 || response.code() == 400)
                            {

                                showProgress(false);
                                Snackbar.make(mLoginFormView, getString(R.string.error_occured), Snackbar.LENGTH_SHORT).show();
                            }
                            else
                            {
                                showProgress(false);
                                Snackbar.make(mLoginFormView, getString(R.string.invalid_credentials), Snackbar.LENGTH_SHORT).show();
                            }
                        }
                    });


                }
            });
        }
    }

    //show the progress circle
    private void showProgress(final Boolean show) {

            long shortAnimTime = getResources().getInteger(android.R.integer.config_shortAnimTime);

            //hide the login form
            mLoginFormView.setVisibility((show)? View.GONE:View.VISIBLE);
            mLoginFormView.animate()
                    .setDuration(shortAnimTime)
                    .alpha((show)? 0.0f:1.0f)
                    .setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    super.onAnimationEnd(animation);
                    mLoginFormView.setVisibility((show)? View.GONE:View.VISIBLE);
                }
            });

            //and show the progress circle
            mProgressView.setVisibility((show)? View.VISIBLE:View.GONE);
            mProgressView.animate()
                    .setDuration(shortAnimTime)
                    .alpha((show)? 1.0f:0.0f)
                    .setListener(new AnimatorListenerAdapter() {
                        @Override
                        public void onAnimationEnd(Animator animation) {
                            super.onAnimationEnd(animation);
                            mProgressView.setVisibility((show)? View.VISIBLE:View.GONE);
                        }
                    });
    }

    //check if email is valid
    private boolean isEmailValid(String email) {
        Pattern pattern = Patterns.EMAIL_ADDRESS;
        return pattern.matcher(email).matches();
    }

    private void startMainActivity() {
        finish();
        Intent intent = new Intent (this, ControlParkMainActivity.class);
        intent.putExtra("isAdmin", isAdmin);
        startActivity(intent);
    }

}
