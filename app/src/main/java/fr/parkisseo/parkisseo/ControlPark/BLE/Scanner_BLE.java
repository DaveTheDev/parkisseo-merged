package fr.parkisseo.parkisseo.ControlPark.BLE;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothManager;
import android.content.Context;
import android.os.Handler;

import java.util.UUID;


public class Scanner_BLE {

    private BLEMainActivity activity;

    private BluetoothAdapter mBluetoothAdapter;
    private boolean mScanning;
    private Handler mHandler;

    private long scanPeriod;
    private int signalStrength;

    public Scanner_BLE(BLEMainActivity mainActivity, long scanPeriod, int signalStrength) {
        activity = mainActivity;

        mHandler = new Handler();

        this.scanPeriod = scanPeriod;
        this.signalStrength = signalStrength;

        final BluetoothManager bluetoothManager =
                (BluetoothManager) activity.getSystemService(Context.BLUETOOTH_SERVICE);
        mBluetoothAdapter = bluetoothManager.getAdapter();
    }

    public boolean isScanning() {
        return mScanning;
    }

    public void start() {
        if (!Utils.checkBluetooth(mBluetoothAdapter)) {
            Utils.requestUserBluetooth(activity);
            activity.stopScan();
        }
        else {
            scanLeDevice(true);
        }
    }

    public void stop() {
        scanLeDevice(false);
    }


    //start scanning for BLE devices
    private void scanLeDevice(final boolean enable) {
        if (enable && !mScanning) {
            Utils.toast(activity.getApplicationContext(), "Starting BLE scan...");

            // Stops scanning after a pre-defined scan period.
            mHandler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    Utils.toast(activity.getApplicationContext(), "Stopping BLE scan...");
                    mScanning = false;
                    mBluetoothAdapter.stopLeScan(mLeScanCallback);
                    BLEMainActivity.stopProgressCircle();
                    activity.stopScan();
                }
            }, scanPeriod);

            mScanning = true;
//            mBluetoothAdapter.startLeScan(mLeScanCallback);
            //start scanning for devices with UUIDs
            mBluetoothAdapter.startLeScan(new UUID[]{UUID.fromString("cb120001-ca32-4031-8f91-3546bd912161")},
                    mLeScanCallback);
        }
        else {
            //stop scanning
            mScanning = false;
            mBluetoothAdapter.stopLeScan(mLeScanCallback);
            BLEMainActivity.stopProgressCircle();
        }
    }

    // Device scan callback. Add the newly discovered device to the list
    private BluetoothAdapter.LeScanCallback mLeScanCallback =
            new BluetoothAdapter.LeScanCallback() {

                @Override
                public void onLeScan(final BluetoothDevice device, int rssi, byte[] scanRecord) {

                    final int new_rssi = rssi;
                    if (rssi > signalStrength) {
                        mHandler.post(new Runnable() {
                            @Override
                            public void run() {
                                activity.addDevice(device, new_rssi);
                            }
                        });
                    }
                }
            };
}